---
description: FSC1189 - ALGORITMO E PROGRAMAÇÃO
cover: .gitbook/assets/como-funciona-algoritmo-do-google-1-1024x538.png
coverY: -35.35627081021088
layout:
  cover:
    visible: true
    size: full
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# 📋 Mural

Sumário

1.0 ​​​​​​​Fundamentos de algoritmos\
2.0 Estrutura lógica dos algoritmos\
3.0 Estrutura condicional dos algoritmos\
4.0 Estrutura de repetição dos algoritmos\
5.0 Vetores e Matrizes em algoritmos\
6.0 Modularizacao em algoritmos\
7.0 Tópicos em programacao de computadores

### Referencias

Bibliografia básica introdutória recomendada.

|                                                                                                                          |                                                                                                        |
| ------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------ |
| <p><img src=".gitbook/assets/image.png" alt="" data-size="original"><br>Marco A furlan et al.</p><p>CENGAGE Learning</p> | <p><img src=".gitbook/assets/image (1).png" alt="" data-size="original"><br>João Ribeiro</p><p>GEN</p> |

### Ferramentas

IDE’s (Interfaces de Desenvolvimento)

* [Portugol Webstudio](https://portalfisica.com/portugol-webstudio) - Compilador de Pseudocódigos Portugol Online
* [Codecademy](https://www.codecademy.com/workspaces/6628421fa828d8caa77104bb) – IDE Onlline
* [Workspace do Time FSC1189](https://codesandbox.io/p/sandbox/busy-booth-k5kjwb?file=/main.py) – Codesandbox IDE Onlline
* [Projeto para o Time FSC1189](https://portalfisica.com/replit/) – Replit IDE Onlline

***

## Mapas conceituais

{% embed url="https://xmind.ai/share/nsa6f5f9?xid=Ac3q3V2Q" %}
Algoritmo e Lógica de Programação
{% endembed %}

| Ads                                                                    |                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ---------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| <img src=".gitbook/assets/image (13).png" alt="" data-size="original"> | <p><a href="https://www.amazon.com.br/b?_encoding=UTF8&#x26;camp=1789&#x26;creative=9325&#x26;linkCode=ur2&#x26;linkId=983b43ffb4136f23dd09bd8d6e15278b&#x26;node=7842641011&#x26;tag=zrhans-20">Livros de computação</a></p><p><br><a href="https://www.amazon.com.br/b?_encoding=UTF8&#x26;camp=1789&#x26;creative=9325&#x26;linkCode=ur2&#x26;linkId=983b43ffb4136f23dd09bd8d6e15278b&#x26;node=7842641011&#x26;tag=zrhans-20">Tecnologia e Mídias Digitais</a></p> |

***

```python
codenumber_as_integer = None
while number_as_integer is None:
    try:
        number_as_integer = int(input("Please enter an integer: "))
    except ValueError:
        print("Invalid integer!")
print(f"The integer you entered was {number_as_integer}")
```

## [//portalfisica.com/fs](https://portalfisica.com/fsc1189/)

<figure><img src=".gitbook/assets/image (5).png" alt=""><figcaption></figcaption></figure>

#### Demonstração

Considerando dois vetores, $$\vec{A}(12,15)$$ e $$\vec{B}(-9,1)$$, calcule o vetor resultante $$\vec{V_R}$$.

```scss
programa
{
  inclua biblioteca Util --> util
  inclua biblioteca Matematica --> mat
 
 funcao inicio() {
    inteiro A[2] = {12,5}
    inteiro B[2] = {-9,-1}
    inteiro C[2]
    inteiro Vr
  
  // preenche o vetor
    para (inteiro x1 = 0; x1 < 2; x1++){
      C[x1] = A[x1] + B[x1]
    }

    para (inteiro x1 = 0; x1 < 2; x1++){
      escreva(C[x1]," ")
    }
    
    // calcula o Vetor resultante
    Vr = mat.raiz (mat.potencia(C[0], 2.0) + mat.potencia(C[1], 2.0), 2.0)
    escreva ("\n [A+B]: ",vr)
    }
}
```

***

#### Plano de ensino em Visual

```mermaid
---
config:
  theme: base
  layout: dagre
---
flowchart TB
 subgraph subGraphIdentificacao["I - Identificação"]
        identificacao["**Curso:** 126 - Física - Licenciatura Plena Noturno 
**Disciplina:** FSC1189 - Algoritmo e Programação 
**Turma:** 12 | **Ano/Período:** 2024/2. Semestre 
**Carga horária:** 60h 
**Docente:** Hans Rogério Zimmermann"]
  end
 subgraph subGraphObjetivo["II - Objetivo"]
        objetivo["Compreender a lógica da programação de computadores para elaborar soluções computacionais aplicando conceitos e técnicas da lógica de programação."]
  end
 subgraph subGraphPrograma["III - Programa da Disciplina"]
        ementa["**Ementa:**
        - Fundamentos de algoritmo
        - Estrutura lógica dos algoritmos
        - Estrutura condicional
        - Estrutura de repetição
        - Vetores e matrizes 
        - Modularização 
        - Tópicos em programação de computadores"]
        bibliografia["**Bibliografia:**"]
        basica["**Básica:**"]
        b1["Algoritmos: teoria e prática - Cormen et al."]
        b2["Algoritmos e programação de computadores - Piva Jr. et al."]
        b3["Algoritmos e lógica de programação - Souza et al."]
        complementar["**Complementar:**"]
        c1["Algoritmos estruturados - Farrer et al."]
        c2["Lógica de programação - Forbellone e Eberspächer"]
        c3["Algoritmos e estruturas de dados - Guimarães e Lages"]
        c4["Algoritmos: lógica para desenvolvimento de programação de computadores - Manzano e Oliveira"]
        c5["Estudo dirigido de algoritmos - Manzano e Oliveira"]
  end
 subgraph subGraphEstrategias["IV - Estratégias de Ensino e Aprendizagem"]
        estrategias["**Estratégias:**"]
        e1["Aulas expositivas"]
        e2["Realização de exercícios"]
        e3["Desenvolvimento de códigos computacionais de forma individual e em grupos"]
        cronograma["**Cronograma:**"]
        semana1["**Semana 1:**
        - Aula Teórica: Apresentação e Fundamentos de algoritmos"]
        semana2["**Semana 2:**
        - Aula Teórica: Fundamentos de algoritmos 
        - Prova 01"]
        semana3["**Semana 3:**
        - Aula Teórica: Estrutura lógica dos algoritmos"]
        semana4["**Semana 4:**
        - Avaliação: Estrutura lógica dos algoritmos 
        - *Prova 02*"]
        semana5["**Semana 5:**
        - Aula Teórica: Estrutura condicional"]
        semana6["**Semana 6:**
        - Avaliação: Prova 03"]
        semana7["**Semana 7:**
        - Aula Teórica: Estrutura condicional"]
        semana8["**Semana 8:**
        - Aula Teórica: Estrutura de repetição 
        - _Prova 04_"]
        semana9["**Semana 9:**
        - Aula Teórica: Vetores e matrizes"]
        semana10["**Semana 10:**
        - Aula Teórica: Vetores e matrizes
        - Prova 05"]
        semana11["**Semana 11:**
        - Aula Teórica: Modularização"]
        semana12["**Semana 12:**
        - _Prova 06_
        - Aula Teórica: Modularização"]
        semana13["**Semana 13:**
        - Aula Teórica: Tópicos em programação de computadores"]
        semana14["**Semana 14:**
        - Aula Teórica: Tópicos em programação de computadores"]
        semana15["**Semana 15:**
        - _Prova 07_ 
        - **_Exame_**"]
  end
 subgraph subGraphAvaliacao["V - Avaliação"]
        avaliacao["**Critérios de Avaliação:**"]
        notaPresenca["Nota e Presença Necessária"]
        media["Média >= 7,00"]
        presenca["Presença >= 75%"]
        atividades["Atividades e Provas"]
        atividadesPeso["Atividades: peso 10"]
        provasPeso["Provas: peso 10"]
        calculoNota["Cálculo da Nota Final"]
        notaSemestre{"Nota do Semestre >= 7,00"}
        exame["Exame"]
        mediaFinal{"Média das Notas > 5,00"}
        reprovado["Reprovado"]
        aprovado["**Aprovado**"]
  end
 subgraph subGraphInformacaoComplementares["VI - Informações Complementares"]
        info["**Ferramentas de Apoio:**"]
        plataforma["**Plataforma IDE:** 
            portugol.dev"]
        fluxogramas["**Fluxogramas:** 
        - Mermaid.live
        - Draw.io"]
        colab["**IDE Complementares:** 
        - Google Colab
        - CODESANDBOX 
        - Codecademy 
        - Javascript PlayGround"]
  end


    identificacao --> objetivo
    objetivo --> subGraphPrograma
    subGraphPrograma --> subGraphEstrategias
    subGraphEstrategias --> subGraphAvaliacao
    subGraphAvaliacao --> subGraphInformacaoComplementares
    bibliografia --> basica & complementar
    basica --> b1 & b2 & b3
    complementar --> c1 & c2 & c3 & c4 & c5
    estrategias --> e1 & e2 & e3
    cronograma --> semana1 & semana2 & semana3 & semana4 & semana5 & semana6 & semana7 & semana8 & semana9 & semana10 & semana11 & semana12 & semana13 & semana14 & semana15
    avaliacao --> notaPresenca & atividades & calculoNota
    notaPresenca --> media & presenca
    atividades --> atividadesPeso & provasPeso
    calculoNota --> notaSemestre
    notaSemestre -- Sim --> aprovado
    notaSemestre -- Não --> exame
    exame --> mediaFinal
    mediaFinal -- Sim --> aprovado
    mediaFinal -- Não --> reprovado
    info --> plataforma & fluxogramas & colab
     bibliografia:::Rose
     basica:::Rose
     b3:::Rose
     cronograma:::Rose
     avaliacao:::Rose
     notaPresenca:::Rose
     media:::Rose
     presenca:::Rose
     reprovado:::Rose
     aprovado:::Pine
     info:::Rose
     plataforma:::Rose
     fluxogramas:::Rose
    classDef Pine stroke-width:1px, stroke-dasharray:none, stroke:#254336, fill:#27654A, color:#FFFFFF
    classDef Rose stroke-width:1px, stroke-dasharray:none, stroke:#FF5978, fill:#FFDFE5, color:#8E2236
```

***

```
<script>
        (function(w,d,u){
                var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.com.br/b32338997/crm/site_button/loader_1_npnbnn.js');
</script>
```

\
&#x20;       (function(w,d,u){\
&#x20;               var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);\
&#x20;               var h=d.getElementsByTagName('script')\[0];h.parentNode.insertBefore(s,h);\
&#x20;       })(window,document,'https://cdn.bitrix24.com.br/b32338997/crm/site\_button/loader\_1\_npnbnn.js');
