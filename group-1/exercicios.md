---
description: Portalfisica.com -  Site Acadêmico de Física na Net
---

# Exercícios

### Exercícios <a href="#exercicios-1" id="exercicios-1"></a>

Esse tutorial é dedicado apenas para exercícios para colocar em prática vários conceitos que você viu nos tutoriais anteriores. Os exercícios são da [OBI](https://olimpiada.ic.unicamp.br/).

A OBI (Olímpiada Brasileira de Informática) é uma competição de informática organizada pelo [Instituto de Computação da UNICAMP](http://www.ic.unicamp.br/) que reúne alunos do Ensino Fundamental ao Ensino Superior. Ela é dividida em duas modalidades: iniciação e programação. A iniciação é apenas para alunos do Ensino Fundamental. A programação é para alunos do Ensino Fundamental ao Ensino Superior. Ambas são divididas em vários níveis. Veja mais informações [aqui](https://olimpiada.ic.unicamp.br/info/).

A página da OBI armazena e disponibiliza publicamente exercícios de edições anteriores. Abaixo, estão alguns exercícios do **Nível Júnior** da OBI com soluções comentadas. Convido você a fazer os exercícios para por em prática o que você aprendeu nos tutoriais. Cada exercício terá um link para a página da OBI que disponibiliza o exercício. Nessa página, você pode enviar a sua solução em uma linguagem de programação, caso você já programe em alguma linguagem que é aceita pela OBI. A correção é automática.

Os exercícios da OBI contém uma seção com restrições, que se referem às restrições dos dados que o usuário fornece. Essas restrições foram ignoradas, para facilitar a resolução dos exercícios. Além disso, os exercícios também tem uma seção com entradas do usuário. Sugiro testar se o seu algoritmo está correto pelo menos com essas entradas, verificando se o seu programa tem as mesmas saídas mostradas na página do exercício.

Alguns exercícios tiveram pequenas modificações em relação aos originais.

### Busca na Internet <a href="#busca-na-internet" id="busca-na-internet"></a>

João fez uma pesquisa em seu site de busca predileto, e encontrou a resposta que estava procurando no terceiro link listado. Além disso, ele viu, pelo site, que _t_ pessoas já haviam clicado neste link antes. João havia lido anteriormente, também na Internet, que o número de pessoas que clicam no segundo link listado é o dobro de número de pessoas que clicam no terceiro link listado. Nessa leitura, ele também descobriu que o número de pessoas que clicam no segundo link é a metade do número de pessoas que clicam no primeiro link. João está intrigado para saber quantas pessoas clicaram no primeiro link da busca, e, como você é amigo dele, quer sua ajuda nesta tarefa.

*   [Entrada](https://devschannel.com/logica-de-programacao/exercicios#entrada-busca-internet)

    Cada caso de teste possui apenas um número, t, que representa o número de pessoas que clicaram no terceiro link da busca.
*   [Saída](https://devschannel.com/logica-de-programacao/exercicios#saida-busca-internet)

    Para cada caso de teste imprima apenas uma linha, contendo apenas um inteiro, indicando quantas pessoas clicaram no primeiro link, nessa busca.
*
*   [Solução](https://devschannel.com/logica-de-programacao/exercicios#solucao-busca-internet)

    Sabemos que o número de pessoas que clicaram no terceiro link é t, e que o número de pessoas que clicaram no segundo link é o dobro, ou seja, _2t_. Sabemos também que o número de pessoas que clicaram no primeiro link é o dobro do número de pessoas que clicaram no segundo link, ou seja, _4t_. Assim, para saber o número de pessoas que clicaram no primeiro link, basta multiplicar _t_ por _4_.

    ```
    inteiro t
    leia(t)
    escreva(t * 4)
    ```

### Campeonato <a href="#campeonato" id="campeonato"></a>

Dois times, Cormengo e Flaminthians, participam de um campeonato de futebol, juntamente com outros times. Cada vitória conta três pontos, cada empate um ponto. Fica melhor classificado no campeonato um time que tenha mais pontos. Em caso de empate no número de pontos, fica melhor classificado o time que tiver maior saldo de gols. Se o número de pontos e o saldo de gols forem os mesmos para os dois times então os dois times estão empatados no campeonato. Dados os números de vitórias e empates, e os saldos de gols dos dois times, sua tarefa é determinar qual dos dois está melhor classificado, ou se eles estão empatados no campeonato.

Veja a [página do exercício ](https://olimpiada.ic.unicamp.br/pratique/pj/2012/f1/campeonato/)na OBI aqui.

[Entrada](https://devschannel.com/logica-de-programacao/exercicios#entrada-campeonato)

A entrada contém seis inteiros: Cv, Ce, Cs, Fv, Fe e Fs, que são, respectivamente, o número de vitórias do Cormengo, o número de empates do Cormengo, o saldo de gols do Cormengo, o número de vitórias do Flaminthians, o número de empates do Flaminthians e o saldo de gols do Flaminthians.

[Saída](https://devschannel.com/logica-de-programacao/exercicios#saida-campeonato)

Seu programa deve imprimir uma única linha. Se Cormengo é melhor classificado que Flaminthians, a linha deve conter apenas a letra _C_, se Flaminthians é melhor classificado que Cormengo, a linha deve conter apenas a letra _F_, e se os dois times estão empatados a linha deve conter apenas o caractere _=_.

[Exemplos](https://devschannel.com/logica-de-programacao/exercicios#exemplos-campeonato)

| Entrada         | Saída |
| --------------- | ----- |
| 10 5 18 11 1 18 | C     |
| 10 5 18 11 2 18 | =     |
| 9 5 -1 10 2 10  | F     |

[Solução](https://devschannel.com/logica-de-programacao/exercicios#solucao-campeonato)

O primeiro passo (depois de declarar as variáveis e ler a entrada do usuário, obviamente), é calcular o número de pontos de cada time. Se os times tiverem o mesmo número de pontos, o melhor classificado é aquele com o maior saldo de gols. Se o saldo de gols for o mesmo, os dois times estão empatados. Se os times não tiverem o mesmo número de pontos, o melhor classificado é aquele que tiver o maior número de pontos.

Pra facilitar, vou armazenar o resultado em uma variável resultado, assumir inicialmente que o Cormengo é o melhor classificado, e cobrir os casos em que o Flaminthians é o melhor classificado e onde os dois times estão empatados.

```
inteiro vitoriasC, empatesC, saldoC, pontosC, vitoriasF, empatesF, saldoF, pontosF
leia(vitoriasC, empatesC, saldoC, vitoriasF, empatesF, saldoF)
inteiro resultado = 0
pontosC = vitoriasC * 3 + empatesC
pontosF = vitoriasF * 3 + empatesF

// Determina o resultado
se (pontosC == pontosF) {
    se (saldoF > saldoC) {
        resultado = 1
    }
    senao se (saldoF == saldoC) {
        resultado = 2
    }
}
senao {
    se (pontosF > pontosC) {
        resultado = 1
    }
}

// Exibe o resultado
se (resultado == 0) {
    escreva("C")
}
senao se (resultado == 1) {
    escreva("F")
}
senao {
    escreva("=")
}
```

### Chocolate <a href="#chocolate" id="chocolate"></a>

Por lei, na Nlogônia todas as barras de chocolate são quadradas. Anamaria tem uma barra quadrada de chocolate de lado _L_, que ela quer compartilhar com alguns colegas da obi. Mas ela é uma boa cidadã e cumpre a lei. Então, ela divide a barra em quatro pedaços quadrados, de lado _L=L/2_. Depois, ela repete esse procedimento com cada pedaço gerado, sucessivamente, enquanto o lado for maior do que, ou igual a _2cm_. Você deve escrever um programa que, dado o lado _L_ da barra inicial, em centímetros, determina quantos pedaços haverá ao final do processo.

Veja a [página do exercício ](https://olimpiada.ic.unicamp.br/pratique/pj/2012/f2/chocolate/)na OBI aqui.

[Entrada](https://devschannel.com/logica-de-programacao/exercicios#entrada-chocolate)

A entrada consiste de uma linha, com um único inteiro, L, o número de centímetros do lado do quadrado.

[Saída](https://devschannel.com/logica-de-programacao/exercicios#saida-chocolate)

Seu programa deve imprimir uma única linha, contendo um único inteiro, igual ao número total de pedaços obtidos pela Anamaria.

[Exemplos](https://devschannel.com/logica-de-programacao/exercicios#exemplos-chocolate)

| Entrada | Saída |
| ------- | ----- |
| 4       | 16    |
| 9       | 64    |
| 2       | 4     |

[Solução](https://devschannel.com/logica-de-programacao/exercicios#solucao-chocolate)

A solução desse problema é bem simples. Inicialmente, você tem a barra inteira. Então, a barra é dividida em 4 pedaços, e depois cada pedaço é dividido em quatro pedaços. Ao final de cada processo, todos os pedaços terão lados iguais. Enquanto o lado deles for maior ou igual a 2, esse processo deve se repetir. A cada divisão, o lado cai pela metade e o número de pedaços é multiplicado por 4.

```
Copyinteiro l, pedacos = 1
leia(l)
enquanto (l >= 2) {
    pedacos*= 4
    l/= 2
}
escreva(pedacos)
```

### Tomadas <a href="#tomadas" id="tomadas"></a>

A Olimpíada Internacional de Informática (IOI, no original em inglês) é a mais prestigiada competição de programação para alunos de ensino médio; seus aproximadamente 300 competidores se reúnem em um país diferente todo ano para os dois dias de prova da competição. Naturalmente, os competidores usam o tempo livre para acessar a Internet, programar e jogar em seus notebooks, mas eles se depararam com um problema: o saguão do hotel só tem uma tomada.

Felizmente, os quatro competidores da equipe brasileira da IOI trouxeram cada um uma régua de tomadas, permitindo assim ligar vários notebooks em uma tomada só; eles também podem ligar uma régua em outra para aumentar ainda mais o número de tomadas disponíveis. No entanto, como as réguas têm muitas tomadas, eles pediram para você escrever um programa que, dado o número de tomadas em cada régua, determina quantas tomadas podem ser disponibilizadas no saguão do hotel.

Veja a [página do exercício](https://olimpiada.ic.unicamp.br/pratique/pj/2013/f1/tomadas/) na OBI aqui.

[Entrada](https://devschannel.com/logica-de-programacao/exercicios#entrada-tomadas)

A entrada consiste de quatro inteiros positivos T\_1, T\_2, T\_3, T\_4, indicando o número de tomadas de cada uma das quatro réguas.

[Saída](https://devschannel.com/logica-de-programacao/exercicios#saida-tomadas)

Seu programa deve imprimir uma única linha contendo um único número inteiro, indicando o número máximo de notebooks que podem ser conectados num mesmo instante.

[Exemplos](https://devschannel.com/logica-de-programacao/exercicios#exemplos-tomadas)

| Entrada | Saída |
| ------- | ----- |
| 2 4 3 2 | 8     |
| 6 6 6 6 | 21    |
| 2 2 2 2 | 5     |

[Solução](https://devschannel.com/logica-de-programacao/exercicios#solucao-tomadas)

Temos quatro réguas de tomadas, e cada uma será conectada na régua seguinte, exceto a última, por não ter nenhuma régua depois dela. Assim, as três primeiras réguas terão uma tomada que será usada apenas para se conectar à outra régua. Então, o número de tomadas livres é a soma do número de tomadas das quatro réguas subtraído pelas três tomadas usadas para conectar outras réguas.

```
Copyinteiro t1, t2, t3, t4
leia(t1, t2, t3, t4)
escreva(t1 + t2 + t3 + t4 - 3)
```

### Volume da TV <a href="#volume-da-tv" id="volume-da-tv"></a>

Bruno é um menino que gosta muito de ver televisão. No entanto ele se depara com um problema muito chato. Sempre que começa um novo programa no canal preferido dele, a TV Nlogônia, acontece de o volume do som deste programa estar diferente do anterior, às vezes com volume menor, outras vezes com volume maior. Quando está com volume menor, ele aumenta o volume pressionando uma quantidade de vezes seguidas o botão de aumentar para o volume ficar ideal; a mesma coisa acontece quando está um volume maior, e ele diminui o volume pressionando alguma quantidade de vezes seguidas o botão de diminuir o volume para ficar com o volume que ele goste no momento.

O aparelho de TV dele tem umas peculiaridades: ele possui volume mínimo, com valor 0 (também chamado de mudo), e volume máximo, com valor 100. A TV nunca ultrapassa os volumes máximo e mínimo. Por exemplo, se o volume já estiver no máximo e ele pressionar o botão de aumentar o som, o volume não se altera. Da mesma forma, se o volume estiver no valor mínimo e ele pressionar o botão de diminuir o som, o volume não se altera.

Agora Bruno quer sua ajuda: ele lembra qual era o volume inicial da TV, e quantas vezes ele pressionou cada botão. Mas, como foram várias mudanças de volume, ele não sabe qual é o volume atual da TV. Por isso, pediu que você o ajude a calcular qual é o volume atual, dados o volume inicial e a lista de trocas de volume que ele realizou.

Veja a [página do exercício](https://olimpiada.ic.unicamp.br/pratique/pj/2013/f2/volume/) na OBI aqui.

[Entrada](https://devschannel.com/logica-de-programacao/exercicios#entrada-volume-tv)

Contém dois números inteiros V e T, que indicam, respectivamente, o volume inicial e o número de trocas de volume. Depois, contém T números inteiros _A\_i_ que mostram as modificações de volume realizadas, na ordem em que estas modificações foram feitas. O primeiro número indica a primeira modificação de volume, o segundo número indica a segunda modificação, e assim por diante. Para cada modificação, um número maior do que zero significa quantas vezes Bruno pressionou o botão de aumentar o som; um número menor do que zero significa quantas vezes ele pressionou o botão de diminuir o som. Ou seja, se o número é igual a 5, significa que nessa modificação ele pressionou cinco vezes o botão de aumentar o som; se o número é igual a -3, significa que nessa modificação ele pressionou o botão de diminuir o som três vezes.

[Saída](https://devschannel.com/logica-de-programacao/exercicios#saida-volume-tv)

Seu programa deve imprimir apenas uma linha, contendo apenas um inteiro F, que indica qual o volume atual da TV após as mudanças de volume.

[Exemplos](https://devschannel.com/logica-de-programacao/exercicios#exemplos-volume-tv)

| Entrada              | Saída |
| -------------------- | ----- |
| 50 4 11 20 -15 -13   | 53    |
| 50 5 30 30 30 40 -78 | 22    |

[Solução](https://devschannel.com/logica-de-programacao/exercicios#solucao-volume-tv)

Após ler as variáveis, o volume final inicialmente é igual ao inicial. Depois, a cada uma das trocas de volume, após ler o valor da troca, o volume final soma o valor da troca. Depois, se o volume ultrapassou algum limite de volume, ele é redefinido para o valor do limite ultrapassado.

```
inteiro vi, troca, trocas, vf
leia(vi, trocas)
vf = vi

para (inteiro i = 0; i < trocas; i++) {
    leia(troca)
    vf+= troca

    se (vf < 0 ) {
        vf = 0
    }
    se (vf > 100) {
        vf = 100
    }
}
escreva(vf)
```

### Prêmio do Milhão <a href="#premio-do-milhao" id="premio-do-milhao"></a>

Alice e Bia criaram uma página na Internet com informações sobre o Macaco-prego-de-peito-amarelo, uma espécie em extinção. A página mostra como todos podem ajudar a manter o habitat natural para evitar que a espécie seja extinta.

Uma empresa gostou tanto da iniciativa de Alice e Bia que prometeu doar um prêmio para que as duas amigas possam realizar outras iniciativas semelhantes. A empresa decidiu que o prêmio seria dado quando a soma do número de acessos à página chegasse a 1 milhão.

Dada a lista de acessos diários que ocorreram à página de Alice e Bia, escreva um programa para determinar quantos dias foram necessários para a soma dos acessos chegar a 1 milhão e as amigas ganharem o prêmio.

Veja a [página do exercício](https://olimpiada.ic.unicamp.br/pratique/pj/2015/f1/premio/) na OBI aqui.

[Entrada](https://devschannel.com/logica-de-programacao/exercicios#entrada-premio-milhao)

Contém um número inteiro N, que indica o número de dias que a lista contém. Cada uma das linhas seguintes contém um único inteiro A, o número de acessos em um dia. O primeiro número dado indica o número de acessos no primeiro dia, o segundo número dado indica o número de acessos no segundo dia, e assim por diante.

[Saída](https://devschannel.com/logica-de-programacao/exercicios#saida-premio-milhao)

Seu programa deve escrever na saída uma única linha, contendo um único número inteiro, o número de dias que foram necessários para a soma dos acessos à pagina de Alice e Bia chegar a 1000000.

[Exemplos](https://devschannel.com/logica-de-programacao/exercicios#exemplos-premio-milhao)

| Entrada                          | Saída |
| -------------------------------- | ----- |
| 5 100 99900 400000 500000 600000 | 4     |
| 1 1000000                        | 1     |

[Solução](https://devschannel.com/logica-de-programacao/exercicios#solucao-premio-milhao)

Assumimos que Alice e Bia sempre ganham o prêmio. Precisamos saber apenas quantos dias foram necessários para isso acontecer. Após ler o número de dias fornecido pelo usuário, basta usar um `for` e ler o número de acessos de cada dia. Precisamos ter uma variável declarada antes do for, que armazene o número de dias necessários para ganhar o prêmio. Essa variável deve ser incrementada em 1 no for enquanto o número de acessos não for pelo menos _1.000.000_. Depois, é só imprimir a variável.

```
Copyinteiro n, acessosDia, acessos = 0
leia(n)
inteiro diasNecessarios = 1

para (inteiro i = 0; i < n; i++) {
    leia(acessosDia)
    acessos+= acessosDia
    se (acessos < 1000000) {
        diasNecessarios++
    }
}

escreva(diasNecessarios)
```

### Código <a href="#codigo" id="codigo"></a>

Gabriel inventou um código para representar números naturais, usando uma sequência de zeros e uns. Funciona assim, o número natural é representado pela quantidade de vezes que o padrão _100_ aparece na sequência. Por exemplo, na sequência _11101001010011110_, o padrão aparece duas vezes; e na sequência _11101010111110111010101_ ele não aparece nenhuma vez. Você deve ajudar Gabriel e implementar um programa que, dada a sequência de zeros e uns, calcule quantas vezes o padrão _100_ aparece nela.

Veja a [página do exercício](https://olimpiada.ic.unicamp.br/pratique/pj/2015/f2/codigo/) na OBI aqui.

[Entrada](https://devschannel.com/logica-de-programacao/exercicios#entrada-codigo)

A entrada contém um inteiro N, o tamanho da sequência, e uma sequência de _N_ zeros e uns.

[Saída](https://devschannel.com/logica-de-programacao/exercicios#saida-codigo)

Seu programa deve imprimir um inteiro, quantas vezes o padrão _100_ aparece na sequência.

[Exemplos](https://devschannel.com/logica-de-programacao/exercicios#exemplos-codigo)

| Entrada                              | Saída |
| ------------------------------------ | ----- |
| 17 1 1 1 0 1 0 0 1 0 1 0 0 1 1 1 1 0 | 2     |
| 8 1 1 1 1 0 1 1 1                    | 0     |
| 3 1 0 0                              | 1     |
| 3 0 1 0                              | 0     |

[Solução](https://devschannel.com/logica-de-programacao/exercicios#solucao-codigo)

Além de armazenar o tamanho da sequência e o número de repetições do padrão, precisamos de um vetor de inteiros que armazene a sequência em si. Depois de ler a sequência fornecida pelo usuário e armazená-la no vetor, deve-se percorrer o vetor até o antepenúltimo item. Não é necessário ir além disso, porque depois do antepenúltimo item é impossível ocorrer o padrão por falta de elementos.

Identificamos o padrão verificando se o elemento atual é igual a _1_, e os dois elementos a seguir são iguais a _0_. Ao identificar o padrão, sabemos que ele só poderá ocorrer de novo três elementos depois. Por isso, o trecho `i+= 2` foi colocado para evitar verificações desnecessárias e melhorar a performance.

```
inteiro n, repeticoes = 0
leia(n)
inteiro sequencia[1000]

para (inteiro i = 0; i < n; i++) {
    leia(sequencia[i])
}

para (inteiro i = 0; i < n - 2; i++) {
    se (sequencia[i] == 1 e sequencia[i + 1] == 0 e sequencia[i + 2] == 0) {
        repeticoes++
        i+= 2
    }
}

escreva(repeticoes)
```

### Lâmpadas <a href="#lampadas" id="lampadas"></a>

Você está de volta em seu hotel na Tailândia depois de um dia de mergulhos. O seu quarto tem duas lâmpadas. Vou chamá-las de _A_ e _B_. No hotel há dois interruptores, que chamaremos de _I1_ e _I2_. Ao apertar _I1_, a lâmpada A troca de estado, ou seja, acende se estiver apagada e apaga se estiver acesa. Se apertar _I2_, ambas as lâmpadas _A_ e _B_ trocam de estado. As lâmpadas inicialmente estão ambas apagadas. Seu amigo resolveu bolar um desafio para você. Ele irá apertar os interruptores em uma certa sequência, e gostaria que você respondesse o estado final das lâmpadas _A_ e _B_.

Veja a [página do exercício ](https://olimpiada.ic.unicamp.br/pratique/pj/2016/f1/lampadas/)na OBI aqui.

[Entrada](https://devschannel.com/logica-de-programacao/exercicios#entrada-lampadas)

A entrada contém um número N que representa quantas vezes seu amigo irá apertar algum interruptor, e _N_ números, que pode ser _1_, se o interruptor _I1_ foi apertado, ou _2_, se o interruptor _I2_ foi apertado.

[Saída](https://devschannel.com/logica-de-programacao/exercicios#saida-lampadas)

Seu programa deve imprimir dois valores, em linhas separadas. Na primeira linha, imprima _verdadeiro_ se a lâmpada _A_ estiver acesa no final das operações e _falso_ caso contrário. Na segunda linha, imprima _verdadeiro_ se a lâmpada _B_ estiver acesa no final das operações e _falso_ caso contrário.

[Exemplos](https://devschannel.com/logica-de-programacao/exercicios#exemplos-lampadas)

| Entrada   | Saída            |
| --------- | ---------------- |
| 3 1 2 2   | verdadeiro falso |
| 4 2 1 2 2 | falso verdadeiro |

[Solução](https://devschannel.com/logica-de-programacao/exercicios#solucao-lampadas)

Depois de declarar as variáveis necessárias, precisamos ler quantas vezes um interruptor será apertado. Depois, deve-se ler qual interruptor é apertado a cada vez. Se for o _1_, deve-se inverter o valor lógico da lâmpada _A_. Se for o _2_, deve-se inverter o valor lógico das duas lâmpadas. No final, deve-se escrever o valor lógico das duas lâmpadas.

```
inteiro n, interruptor
logico la = falso, lb = falso
leia(n)

para (inteiro i = 0; i < n; i++) {
    leia(interruptor)
    se (interruptor == 1) {
        la = nao la
    }
    se (interruptor == 2) {
        la = nao la
        lb = nao lb
    }
}

escreva(la, "\n", lb, "\n")
```

### Medalhas <a href="#medalhas" id="medalhas"></a>

A natação foi um dos esportes mais emocionantes das Olimpíadas do Rio. Houve até uma prova na qual três atletas chegaram empatados, cada um recebendo uma medalha de prata! Normalmente, porém, os três primeiros colocados terminam a prova em tempos distintos e, portanto, temos a distribuição mais comum de medalhas: o nadador que terminou no menor tempo recebe medalha de ouro; o nadador que terminou com o segundo menor tempo recebe medalha de prata; e o que terminou com o terceiro menor tempo recebe medalha de bronze. Neste problema, dados os três tempos distintos de finalização da prova, dos três nadadores que ganharam medalhas, seu programa deve dizer quem ganhou medalha de ouro, quem ganhou prata e quem ganhou bronze.

Veja a [página do exercício ](https://olimpiada.ic.unicamp.br/pratique/pj/2016/f2/medalhas/)na OBI aqui.

[Entrada](https://devschannel.com/logica-de-programacao/exercicios#entrada-medalhas)

A entrada contém um inteiro T1, indicando o tempo em que o nadador _1_ terminou a prova, um inteiro T2, indicando o tempo de finalização do nadador _2_, e um inteiro T3, indicando o tempo em que o nadador _3_ terminou a prova.

[Saída](https://devschannel.com/logica-de-programacao/exercicios#saida-medalhas)

Seu programa deve imprimir três linhas na saída. A primeira linha deve conter o número do nadador que ganhou medalha de ouro; a segunda linha, o número do nadador que ganhou prata; e a terceira linha, o número do nadador que levou bronze.

[Exemplos](https://devschannel.com/logica-de-programacao/exercicios#exemplos-medalhas)

| Entrada     | Saída |
| ----------- | ----- |
| 230 183 234 | 2 1 3 |
| 46 47 48    | 1 2 3 |
| 11 21 7     | 3 1 2 |

[Solução](https://devschannel.com/logica-de-programacao/exercicios#solucao-medalhas)

Existem várias formas de se solucionar esse problema. Uma delas é armazenar a posição e o tempo dos nadadores em uma matriz e ordenar essa matriz pelo tempo, em ordem crescente. Depois da ordenação, basta escrever a posição dos nadadores, já que eles estarão na ordem correta. Como fazer a ordenação? Existem várias formas. Esse é um assunto um pouco complexo para quem estuda Lógica de Programação. Por isso, eu vou usar um dos algoritmos mais simples de ordenação, que é o **Bubble Sort**.

Acho que é melhor explicar o Bubble Sort com um exemplo: imagine que você tenha um vetor de produtos vendidos em um mês e você quer ordená-lo em ordem crescente usando Bubble Sort. Suponhamos que o vetor tenha os elementos _5_, _4_, _7_, _12_, _3_. No Bubble Sort, você compara cada índice do vetor com os índices que estão à frente dele. Se o objetivo é ordenar de forma crescente, você verifica se o elemento do índice atual é maior do que o índice que você está comparando. Se for, você troca os elementos de posição. Você faz isso para todos os elementos, exceto o último, porque ele não tem elementos à frente dele.

Aplicando o conceito explicado acima no exemplo da lista de vendas, primeiramente pegaríamos o primeiro elemento e compararíamos com o segundo. Como o primeiro é maior que o segundo, os dois elementos trocam de posição. Depois comparamos o primeiro com o terceiro. Como o primeiro não é maior que o terceiro, nada acontece. O mesmo acontece na comparação com o quarto elemento. Na comparação com o quinto elemento, como o primeiro é maior que o quinto, os dois trocam de posição. Ao final desse processo, o primeiro elemento será com certeza o menor da lista. Então, não precisamos mais compará-lo a ninguém. Essa parte da lista já está ordenada.

Repetimos o processo feito com o primeiro elemento com os demais elementos, exceto o último. Assim, comparamos o segundo elemento com o terceiro, depois com o quarto e com o quinto, trocando os elementos de posição quando o segundo for maior. E repetimos o mesmo processo com o terceiro e quarto elementos. Ao final de todo o processo, a lista estará completamente ordenada.

Como podemos passar esse conceito para um algoritmo? Precisamos de dois loops. O primeiro é um `para` que percorre todos os elementos do vetor até o penúltimo. O segundo também é um `para`, e fica dentro do primeiro loop. Ele percorre os elementos do vetor a partir do primeiro elemento à direita do elemento atual do primeiro `para` até o último elemento. Aí, dentro dos dois loops, checamos se o elemento do primeiro loop é maior do que o elemento do segundo loop. Se for, os dois trocam de posição. Veja um exemplo completo abaixo:

```
inteiro temp
inteiro vendas[] = {5, 4, 7, 12, 3}

para (inteiro i = 0; i < 4; i++) {
    para (inteiro j = i + 1; j < 5; j++) {
        se (vendas[i] > vendas[j]) {
            temp = vendas[i]
            vendas[i] = vendas[j]
            vendas[j] = temp
        }
    }
}

para (inteiro i = 0; i < 5; i++) {
    escreva(vendas[i], "\n")
}
```

Saí bastante do problema do exercício, né? Fiz isso porque precisava explicar o algoritmo de Bubble Sort para vocês. A explicação foi detalhada, porque sei que é um conceito difícil de entender para iniciantes. Voltando ao exercício em si, o que ele quer é que dados os tempos de 3 nadadores, fornecidos pelo usuário, a gente informe qual é a posição do nadador que ganhou o ouro, a prata e o bronze. Podemos fazer isso usando uma matriz. O bubble sort pode ser usado para ordenar matrizes também.

Vou criar uma matriz com 3 linhas e 2 colunas. Cada linha representará um nadador. A primeira coluna de cada linha representará a posição do nadador. As posições começarão com 1 no primeiro elemento, 2 no segundo e 3 no terceiro. A segunda coluna representará o tempo do nadador. Depois de declarar as variáveis e ler os tempos, precisamos criar os dois loops para percorrer as linhas da matriz. A ordenação será feita pelo tempo do nadador, ou seja, pelo valor da segunda coluna da matriz. Porém, quando for necessário fazer uma troca, as duas colunas devem ser trocadas.

Ao final do processo, a matriz estará ordenada pelo tempo, e com as posições dos nadadores mais rápidos. Então, basta percorrer a matriz e imprimir as posições de cada nadador.

```
inteiro temp
inteiro nadadores[3][2] = {{1, 0}, {2, 0}, {3, 0}}
leia(nadadores[0][1], nadadores[1][1], nadadores[2][1])

para (inteiro i = 0; i < 2; i++) {
    para (inteiro j = i + 1; j < 3; j++) {
        se (nadadores[i][1] > nadadores[j][1]) {
            temp = nadadores[i][0]
            nadadores[i][0] = nadadores[j][0]
            nadadores[j][0] = temp

            temp = nadadores[i][1]
            nadadores[i][1] = nadadores[j][1]
            nadadores[j][1] = temp
        }
    }
}

para (inteiro i = 0; i < 3; i++) {
    escreva(nadadores[i][0], "\n")
}
```

### Bondinho <a href="#bondinho" id="bondinho"></a>

A turma do colégio vai fazer uma excursão na serra e todos os alunos e monitores vão tomar um bondinho para subir até o pico de uma montanha. A cabine do bondinho pode levar 50 pessoas no máximo, contando alunos e monitores, durante uma viagem até o pico. Neste problema, dado como entrada o número de alunos _A_ e o número de monitores _M_, você deve escrever um programa que diga se é possível ou não levar todos os alunos e monitores em apenas uma viagem!

Veja a [página do exercício ](https://olimpiada.ic.unicamp.br/pratique/pj/2017/f1/bondinho/)na OBI aqui.

[Entrada](https://devschannel.com/logica-de-programacao/exercicios#entrada-bondinho)

A entrada contém um inteiro A, representando a quantidade de alunos, e um inteiro M, representando o número de monitores.

[Saída](https://devschannel.com/logica-de-programacao/exercicios#saida-bondinho)

Seu programa deve imprimir uma linha contendo o caractere _S_ se é possível levar todos os alunos e monitores em apenas uma viagem, ou o caractere _N_ caso não seja possível.

[Exemplos](https://devschannel.com/logica-de-programacao/exercicios#exemplos-bondinho)

| Entrada | Saída |
| ------- | ----- |
| 10 20   | S     |
| 12 39   | N     |
| 49 1    | S     |

[Solução](https://devschannel.com/logica-de-programacao/exercicios#solucao-bondinho)

O problema é extremamente simples. Depois de ler o número de alunos e monitores, é só verificar se a soma deles é menor ou igual a 50. Caso seja, deve-se imprimir _S_. Caso contrário, deve-se imprimir _N_.

```
inteiro a, m
leia(a, m)

se (a + m <= 50) {
escreva("S")
}
senao {
escreva("N")
}
```

### Cartas <a href="#cartas" id="cartas"></a>

Um novo game de realidade aumentada tem, dentro dele, um minigame que aparece em certas situações para aumentar o ganho de pontos do game principal. O minigame é um joguinho de memória com quatro cartas, formando dois pares de cartas iguais. Quer dizer, duas cartas têm um número inteiro _N_ marcado em uma de suas faces e as outras duas cartas têm um outro número inteiro _`M`_`,`` `_`N != M`_. Neste problema, o jogador já virou três cartas.

Claro que, dadas as condições, a carta que falta virar vai formar par com uma das três que já foram viradas. Implemente um programa que, dados os números de três cartas, imprima o número da carta que ainda falta virar!

Veja a [página do exercício](https://olimpiada.ic.unicamp.br/pratique/pj/2017/f2/cartas/) na OBI aqui.

[Entrada](https://devschannel.com/logica-de-programacao/exercicios#entrada-cartas)

A entrada contém um inteiro A, representando a primeira carta aberta, um inteiro B, representando a segunda carta, e um inteiro C, que é a terceira carta.

[Saída](https://devschannel.com/logica-de-programacao/exercicios#saida-cartas)

Seu programa deve imprimir uma linha contendo um inteiro representando o número que está na carta que ainda falta virar.

[Exemplos](https://devschannel.com/logica-de-programacao/exercicios#exemplos-cartas)

| Entrada  | Saída |
| -------- | ----- |
| 40 11 40 | 11    |
| 8 8 96   | 96    |

[Solução](https://devschannel.com/logica-de-programacao/exercicios#solucao-cartas)

Se a primeira carta for diferente da segunda e da terceira, então ela só aparece uma vez, e por isso é ela que falta. Caso contrário, a carta que falta virar é a que for diferente da primeira.

```
Copyinteiro a, b, c
leia(a, b, c)

se (a != b e a != c) {
    escreva(a)
}
senao {
    se (a != b) {
        escreva(b)
    }
    senao {
        escreva(c)
    }
}
```

### Álbum da copa <a href="#album-da-copa" id="album-da-copa"></a>

Em ano de Copa do Mundo de Futebol, o álbum de figurinhas oficial é sempre um grande sucesso entre crianças e também entre adultos. Para quem não conhece, o álbum contém espaços numerados de _1_ a _N_ para colar as figurinhas; cada figurinha, também numerada de _1_ a _N_, é uma pequena foto de um jogador de uma das seleções que jogará a Copa do Mundo. O objetivo é colar todas as figurinhas nos respectivos espaços no álbum, de modo a completar o álbum (ou seja, não deixar nenhum espaço sem a correspondente figurinha).

As figurinhas são vendidas em envelopes fechados, de forma que o comprador não sabe quais figurinhas está comprando, e pode ocorrer de comprar uma figurinha que ele já tenha colado no álbum.

Para ajudar os usuários, a empresa responsável pela venda do álbum e das figurinhas quer criar um aplicativo que permita gerenciar facilmente as figurinhas que faltam para completar o álbum e está solicitando a sua ajuda.

Dados o número total de espaços e figurinhas do álbum, e uma lista das figurinhas já compradas (que pode conter figurinhas repetidas), sua tarefa é determinar quantas figurinhas faltam para completar o álbum.

Veja a [página do exercício](https://olimpiada.ic.unicamp.br/pratique/pj/2018/f1/album/) na OBI aqui.

[Entrada](https://devschannel.com/logica-de-programacao/exercicios#entrada-album-copa)

A primeira linha contém um inteiro N indicando o número total de figurinhas e espaços no álbum. A segunda linha contém um inteiro M indicando o número de figurinhas já compradas. Cada uma das _M_ linhas seguintes contém um número inteiro X indicando uma figurinha já comprada.

[Saída](https://devschannel.com/logica-de-programacao/exercicios#saida-album-copa)

Seu programa deve produzir uma única linha contendo um inteiro, o número de figurinhas que falta para completar o álbum.

[Exemplos](https://devschannel.com/logica-de-programacao/exercicios#exemplos-album-copa)

| Entrada         | Saída |
| --------------- | ----- |
| 10 3 5 8 3      | 7     |
| 5 6 3 3 2 3 3 3 | 3     |
| 3 4 2 1 3 3     | 0     |

[Solução](https://devschannel.com/logica-de-programacao/exercicios#solucao-album-copa)

O que precisamos imprimir na saída é o número de figurinhas restantes. Inicialmente, esse número é o total de figurinhas. A cada leitura de figurinha comprada, esse número deve ser diminuido em 1, a não ser que a figurinha já tenha sido comprada. Para saber se a figurinha já foi comprada, podemos armazenar as figurinhas compradas em um vetor lógico. A cada vez que uma figurinha for comprada, definimos o índice da figurinha com o valor verdadeiro, caso ele seja falso, e diminuimos o número de figurinhas restantes em 1. Caso ele já seja verdadeiro, significa que a figurinha é repetida, então não fazemos nada.

```
inteiro n, m, indiceFigurinha, figurinhasRestantes
logico figurinhas[100]
leia(n, m)
figurinhasRestantes = n
para (inteiro i = 0; i < m; i++) {
    leia(indiceFigurinha)
    se (figurinhas[indiceFigurinha] == falso) {
        figurinhas[indiceFigurinha] = verdadeiro
        figurinhasRestantes--
    }
}
escreva(figurinhasRestantes)
```
