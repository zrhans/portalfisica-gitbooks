# Table of contents

* [📋 Mural](README.md)
* [📖 1.0 ​​​​​​​Fundamentos de algoritmos](1.0-fundamentos-dee-algoritmos.md)
* [📖 2.0 Estrutura lógica dos algoritmos](2.0-estrutura-logica-dos-algoritmos.md)
* [📖 3.0 Estrutura condicional dos algoritmos](3.0-estrutura-condicional-dos-algoritmos.md)
* [📖 4.0 Estrutura de repeticao dos algoritmos](4.0-estrutura-de-repeticao-dos-algoritmos.md)
* [📖 5.0 Estrutura de Dados dos Algoritmos](5.0-vetores-e-matrizes-em-algoritmossite-academico-de-fisica-na-net/README.md)
  * [Exercícios - Vetores](5.0-vetores-e-matrizes-em-algoritmossite-academico-de-fisica-na-net/exercicios-vetores.md)
* [📖 6.0 Modularização em algoritmos](6.0-modularizacao-em-algoritmos/README.md)
  * [📑 Subprogramas](6.0-modularizacao-em-algoritmos/subprogramas.md)
* [💻7.0 Tópicos em programação de computadores](7.0-topicos-em-programacao-de-computadores.md)

## EXTRAS

* [Uma visão geral sobre lógica de programação age](extras/uma-visao-geral-sobre-logica-de-programacao-age.md)
* [Ferramentas Online](extras/ferramentas-online/README.md)
  * [Runjs.app](https://runjs.app/play)
  * [Playcode.io](https://playcode.io/javascript)
  * [Portugol Webstudio](https://portugol.dev/)
  * [Draw.io](https://app.diagrams.net/)
  * [Mermaid](https://mermaid.live/)
* [Guia completo de fluxograma com exemplos](extras/guia-completo-de-fluxograma-com-exemplos.md)
* [Exercícios](extras/exercicios.md)
* [Estruturas de Dados](extras/page.md)
* [✨Gamma - Vetores e Matrizes](https://gamma.app/docs/s1dfpd8exiu9ugs?following_id=3hwl98jcbusv2b1\&follow_on_start=true)

## 🎞️ Norma ISO 5807-1985(N)

* [1 - Introdução](norma-iso-5807-1985-n/1-introducao.md)
* [2 - Simbologia de dados](norma-iso-5807-1985-n/2-simbologia-de-dados.md)
* [3 - Simbologia de processamento](norma-iso-5807-1985-n/3-simbologia-de-processamento.md)
* [4 - Simbologia de linha](norma-iso-5807-1985-n/4-simbologia-de-linha.md)
* [5 - Simbologia especial](norma-iso-5807-1985-n/5-simbologia-especial.md)
* [6 - Simbologia IBM](norma-iso-5807-1985-n/6-simbologia-ibm.md)
* [7 - Gabarito técnico](norma-iso-5807-1985-n/7-gabarito-tecnico.md)
* [ChatGPT treinado](norma-iso-5807-1985-n/chatgpt-treinado.md)

## Projetos de Classe

* [Estatística sobre LDR](projetos-de-classe/estatistica-sobre-ldr.md)
