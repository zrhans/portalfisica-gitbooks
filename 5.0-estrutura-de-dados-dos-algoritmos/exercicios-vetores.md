---
description: Exercícios de fixação
---

# Exercícios - Vetores

## Vetores

_**Fonte:** Adaptado de Rafael Helerbrock **-**_ [_**https://mundoeducacao.uol.com.br/fisica/vetores**_](https://mundoeducacao.uol.com.br/fisica/vetores)

***

Simbólicamente, como grafo, Vetores são segmentos de retas usados para representar alguma grandeza vetorial.

<figure><img src="https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo_legenda/15602010435cfec753ede04-puxando-e-empurrando.jpg" alt="Ilustração de um homem puxando e outro empurrando um caixote em referência aos vetores." height="315" width="700"><figcaption><p>Apesar de ambas ações precisarem de força, puxar e empurrar são coisas distintas, uma vez que a força é representada por vetores.</p></figcaption></figure>

**Vetor** é um segmento de reta orientado que apresenta módulo (tamanho), direção e sentido. Os vetores são **usados para expressar grandezas físicas vetoriais**, ou seja, aquelas que só podem ser completamente definidas se conhecemos o seu valor numérico, a direção em que atuam (horizontal e vertical), bem como o seu o sentido (para cima, para baixo).

Posição, velocidade, aceleração, força e quantidade de movimento são bons exemplos de [grandezas vetoriais](https://mundoeducacao.uol.com.br/fisica/grandezas-escalares-grandezas-vetoriais.htm). Por exemplo, se quisermos saber a posição de algum local, é necessário que se aponte para uma direção. Nesse caso, o sentido do movimento é dado pela ponta do dedo.

<figure><img src="https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/vetor(1).jpg" alt="Figura mostra um vetor de módulo (tamanho) a." height="311" width="346"><figcaption><p>Figura mostra um vetor de módulo (tamanho) a.</p></figcaption></figure>

Para desenharmos vetores, é necessário perceber que sua representação deve levar em conta o seu tamanho, ou seja, um vetor que represente uma grandeza de valor numérico igual a 10 deve ser desenhado com a metade do tamanho de um vetor que tenha tamanho 20.

<figure><img src="https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/modulo-dos-vetores.jpg" alt="Ilustração de vetores de tamanhos diferentes." height="298" width="700"><figcaption><p>O tamanho em que desenhamos um vetor representa o seu módulo.</p></figcaption></figure>

As **direções de um vetor podem ser definidas com base no sistema de coordenadas escolhido**, por exemplo. Usando-se o[ sistema cartesiano](https://mundoeducacao.uol.com.br/matematica/plano-cartesiano.htm), as direções do espaço seriam $$x$$ e $$y$$ e um vetor poderia ser escrito como $$\mathbf{V} = (x, y)$$. O sentido, por sua vez, diz respeito à seta na ponta do vetor, que o indica, podendo ser tanto positivo como negativo.

![Representação das direções dos vetores.](https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/direcoes.jpg)

Quando escrevemos que um vetor é definido por suas coordenadas  $$x$$ e $$y$$, dizemos que  $$x$$ e $$y$$ são as suas componentes "horizontal" e "vertical", respectivamente. Quando um vetor encontra-se inclinado, sem coincidir com qualquer um dos eixos do sistema de coordenadas, é possível determinar o tamanho das suas [componentes](https://mundoeducacao.uol.com.br/fisica/decomposicao-vetores.htm). Para tanto, basta conhecermos o ângulo $$θ$$, formado entre o vetor e a direção "horizontal", e o módulo do vetor $$\mathbf a$$:

![Representação gráfica de vetores.](https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/componentes-do-vetor.jpg)

Para calcularmos essas componentes, é necessário fazer o seguinte cálculo:

![](https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/formula-componentes-do-vetor\(1\).jpg)

Com base nas componentes $$\mathbf a_x$$ e $$\mathbf a_y$$ de um vetor, é possível calcular o seu módulo (tamanho). Para isso, basta aplicarmos o [teorema de Pitágoras](https://mundoeducacao.uol.com.br/matematica/o-teorema-pitagoras-aplicado-no-estudo-trigonometria.htm), uma vez que essas componentes são perpendiculares entre si:

![](https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/formula-do-modulo-do-vetor.jpg)

### **Vetor resultante**

**Vetor** **resultante** é o nome dado ao vetor que se obtém após realizar-se uma [soma vetorial](https://mundoeducacao.uol.com.br/fisica/soma-vetores.htm). Na soma vetorial, devemos considerar o **módulo**, a **direção** e o **sentido** dos vetores para encontrarmos o vetor resultante. Vejamos, a seguir, alguns casos de operações com vetores.

### **Operações com vetores**

#### → **Soma de vetores**

Vetores paralelos são aqueles que se encontram na mesma direção e no mesmo sentido. O ângulo formado entre esses vetores é sempre nulo. Observe a figura abaixo:

![Vetores paralelos](https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/vetores-paralelos.jpg)

Caso esses vetores tenham também o mesmo módulo, dizemos que se trata de vetores iguais. Para encontrarmos a resultante desses vetores, basta somarmos o módulo de cada um, além disso, o vetor resultante estará na mesma direção e sentido dos vetores paralelos, e seu tamanho deverá ser o tamanho dos dois vetores originários:

![Vetores paralelos](https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/vetor-resultante-paralelos.jpg)

Para calcularmos o módulo do vetor **R**, podemos utilizar a seguinte fórmula:

![](https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/resultante-paralelo.jpg)

#### → **Subtração de vetores**

Vetores opostos fazem um ângulo de 180º entre si, encontram-se na mesma direção, porém com sentidos contrários, como mostra a figura:

![Vetores em sentidos contrários](https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/vetores-opostos.jpg)

O vetor resultante de dois vetores opostos é dado pela diferença no módulo desses, como é possível ver na figura seguinte:

![Vetores em sentidos opostos](https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/vetor-resultante-opostos.jpg)

Nesse caso, o vetor resultante terá sua direção e sentido determinados pelo vetor de maior módulo e poderá ser calculado por meio da seguinte fórmula:

![](https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/resultante-oposto.jpg)

&#x20;

#### → **Vetores perpendiculares: Teorema de Pitágoras**

Vetores perpendiculares formam um ângulo de 90º entre si. Para encontrarmos o vetor resultante de dois vetores perpendiculares, devemos ligar o início de um dos vetores à ponta do outro. O vetor resultante, nesse caso, formará a hipotenusa de um triângulo retângulo, observe:

![Vetores perpendiculares](https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/vetor-resultante-perpendiculares\(1\).jpg)

O módulo desse vetor resultante pode ser calculado usando o teorema de Pitágoras:

![](https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/resultante-perpendicular.jpg)

#### → **Vetores oblíquos: regra do paralelogramo**&#x20;

Vetores que não se encaixem em nenhum dos casos anteriores podem ser determinados geometricamente pela regra do paralelogramo, como na próxima figura:

![Vetores oblíquos](https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/vetor-resultante-obliquos.jpg)

Sendo θ o ângulo formado entre os dois vetores de base (azul e vermelho), o módulo do vetor resultante poderá ser obtido por meio da próxima fórmula:

![](https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/resultante-obliquo.jpg)

#### → **Resultante de vários vetores**

Quando temos diversos vetores e queremos encontrar o vetor resultante, devemos conectá-los uns aos outros. Nesse processo, que independe da ordem escolhida, devemos ligar a ponta de um vetor ao início do próximo. No fim, o vetor resultante será aquele que liga o início do primeiro vetor com a ponta do último:

![Resultante de vários vetores](https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/resultante-de-varios-vetores.jpg)

Para encontrarmos o módulo desse vetor, somamos as componentes x e y de cada um dos vetores **a**, **b**, **c**, e **d**, e, no fim, aplicamos o Teorema de Pitágoras.

![](https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/vetor-resultante-varios-vetores\(1\).jpg)

***

<img src="https://files.oaiusercontent.com/file-gk3ACPm7Tvy5DHe5aE9fqJ0W?se=2024-10-31T03%3A34%3A06Z&#x26;sp=r&#x26;sv=2024-08-04&#x26;sr=b&#x26;rscc=max-age%3D604800%2C%20immutable%2C%20private&#x26;rscd=attachment%3B%20filename%3DFrame%2520612.png&#x26;sig=VeupQw4BNnvEtTFr3eRjXHAni3xM8B7ZID7CPzeSdr8%3D" alt="GPT" data-size="line"> Para expressar a soma de vetores na forma de somatório, considere dois vetores  $$\mathbf{A}$$ e $$\mathbf{B}$$ com componentes $$A_i$$ e $$B_i$$​, respectivamente. A soma desses vetores $$\mathbf{C} = \mathbf{A} + \mathbf{B}$$ também é um vetor, onde cada componente $$C_i$$​ é a soma dos componentes correspondentes de $$\mathbf{A}$$ e $$\mathbf{B}$$.

A soma pode ser representada assim:

$$\mathbf{C} = \sum_{i=1}^{n} (A_i + B_i) \, \mathbf{e}_i$$&#x20;

ou, para o somatório de componentes:

$$
\mathbf{C} = \left( \sum_{i=1}^{n} A_i \right) + \left( \sum_{i=1}^{n} B_i \right)
$$

onde:

* $$i$$ indica o índice de cada componente do vetor.
* $$n$$ é o número de componentes nos vetores.
* $$\mathbf{e}_i$$​ é o vetor unitário na direção do $$i-ésimo$$ componente (em notação vetorial).

#### Exemplo

Para vetores em 3D, $$\mathbf{A} = (A_1, A_2, A_3)$$ e $$\mathbf{B} = (B_1, B_2, B_3)$$



$$\mathbf{C} = \sum_{i = 1}^{3}  (A_i + B_i) = (A_1 + B_1) \mathbf{i} + (A_2 + B_2) \mathbf{j} + (A_3 + B_3) \mathbf{k}$$

Assim, a notação de somatório é uma forma conveniente de expressar a soma dos vetores em suas componentes individuais.

**Teste:**&#x20;

Vamos fazer dois exemplos exemplos e resolver com números inteiros.

Considere dois vetores em 3D:

$$\mathbf{A} = (2, 5, -3)  \\ \mathbf{B} = (4, -1, 6)$$

Queremos encontrar o vetor soma $$\mathbf{C} = \mathbf{A} + \mathbf{B}$$.

#### Passo 1: Definir a soma em termos de somatório

A fórmula para a soma de vetores em componentes usando somatório é:

$$\mathbf{C} = \sum_{i=1}^{3} (A_i + B_i) \mathbf{e}_i$$

onde $$A_i$$​ e $$B_i$$​ são as componentes de  $$\mathbf{A}$$ e $$\mathbf{B}$$, respectivamente, e $$\mathbf{e}_i$$​ são os vetores unitários $$\mathbf{i}$$, $$\mathbf{j}$$, e $$\mathbf{k}$$ para as direções $$x$$, $$y$$, e $$z$$.

#### Passo 2: Somar as componentes de cada direção

Agora somamos cada componente correspondente dos vetores $$\mathbf{A}$$ e $$\mathbf{B}$$:

1. **Componente** $$x$$ **:** $$A_1 + B_1 = 2 + 4 = 6$$
2. **Componente** $$y$$ **:** $$A_2 + B_2 = 5 + (-1) = 4$$
3. **Componente** $$z$$ **:** $$A_3 + B_3 = -3 + 6 = 3$$

#### Passo 3: Escrever o vetor soma

Assim, o vetor$$\mathbf{C} = \mathbf{A} + \mathbf{B}$$ é:

$$\mathbf{C} = (6, 4, 3)$$

Ou, em notação vetorial com unitários:

$$\mathbf{C} = 6 \mathbf{i} + 4 \mathbf{j} + 3 \mathbf{k}$$

#### <mark style="background-color:green;">Resumo</mark>

Portanto, a soma dos vetores $$\mathbf{A} = (2, 5, -3)$$ e $$\mathbf{B} = (4, -1, 6)$$ resulta no vetor:

$$\mathbf{C} = (6, 4, 3)$$

### **Atividade: Estude os exercícios resolvidos sobre vetores e desenvolva:**

1. Um algoritmo
2. Um fluxograma
3. Uma implementação (portugol ou outra linguagem de programação)

### Exercícios resolvidos

**1**) Assinale a alternativa que apresenta corretamente o módulo da resultante de dois vetores, **A** e **B**, cujas componentes são dadas por **A** = (12,5) e **B** = (-9,-1).

a) 12

b) 4

c) 6

<mark style="background-color:blue;">d) 5</mark>

e) 3

**Gabarito:** Letra D

**Resolução:**

Para determinarmos o vetor resultante dos vetores $$\mathbf A$$ e $$\mathbf B$$, precisamos somar suas componentes  $$x$$ e  $$y$$, para tanto, faremos o seguinte cálculo:

![](https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/resultante-exercicio-1.jpg)

De acordo com o resultado encontrado, o vetor resultante é dado $$\mathbf {V_R} = (3,4)$$ e seu módulo vale 5.

**2)** Dois vetores, de módulos iguais a 3 e 2, formam entre si um ângulo de 60º. Determine o módulo da resultante desses vetores.

a) 6

b) √6

c) 5

d) √19

<mark style="background-color:blue;">e) 5</mark>

**Gabarito:** Letra E

**Resolução:**

Para calcularmos o módulo do vetor resultante entre esses dois vetores oblíquos, é necessário utilizarmos a lei dos cossenos, considerando que o ângulo entre esses vetores é 60º. Dessa forma, teremos que fazer o seguinte cálculo:

![](https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/calculo-resultante-exercicio.jpg)

**3)** Um vetor **A,** de módulo 5, encontra-se inclinado com ângulo de 30º em relação ao eixo horizontal. Determine o módulo das componentes horizontal e vertical, $$A_x$$ e $$A_y$$**,** desses vetores.

a) √3 e √2

<mark style="background-color:blue;">b) 5√3/2 e 5/2</mark>

c) 5/2 e 5

d) 3/4 e 5/2

e) 25 e √2

**Gabarito:** Letra B

**Resolução:**

Para determinarmos quais são as componentes do vetor $$A$$, devemos utilizar as relações do seu módulo com o seno e o cosseno do ângulo de 30º, que esse vetor forma com a direção $$x$$. Para tanto, devemos fazer o seguinte cálculo:

![](https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo/componentes-exercicio-3.jpg)

**4)** Vamos considerar três vetores em 3D:

$$\mathbf{A} = (3, -2, 5) \\ \mathbf{B} = (4, 1, -3) \\ \mathbf{C} = (-1, 3, 2)$$

Queremos encontrar o vetor soma $$\mathbf{R} = \mathbf{A} + \mathbf{B} + \mathbf{C}$$.

#### Passo 1: Definir a soma em termos de somatório

Para somar os vetores, usamos a seguinte expressão:

$$\mathbf{R} = \sum_{i=1}^{3} (A_i + B_i + C_i) \mathbf{e}_i$$

onde $$A_i$$​, $$B_i$$​, e $$C_i$$​ são as componentes dos vetores $$\mathbf{A}$$, $$\mathbf{B}$$, e $$\mathbf{C}$$, respectivamente, e $$\mathbf{e}_i$$ são os vetores unitários $$\mathbf{i}$$, $$\mathbf{j}$$, e $$\mathbf{k}$$ para as direções $$x$$$$x$$, $$y$$, e $$z$$.

#### Passo 2: Somar as componentes de cada direção

Agora, somamos cada componente correspondente dos vetores $$\mathbf{A}$$, $$\mathbf{B}$$, e $$\mathbf{C}$$:

1. **Componente** $$x$$ **:** $$A_1 + B_1 + C_1 = 3 + 4 + (-1) = 6$$
2. **Componente** $$y$$ **:** $$A_2 + B_2 + C_2 = -2 + 1 + 3 = 2$$
3. **Componente** $$z$$ **:** $$A_3 + B_3 + C_3 = 5 + (-3) + 2 = 4$$

#### Passo 3: Escrever o vetor soma

Assim, o vetor resultante $$\mathbf{R} = \mathbf{A} + \mathbf{B} + \mathbf{C}$$ será:

$$\mathbf{R} = (6, 2, 4)$$

ou, em notação vetorial com unitários:

$$\mathbf{R} = 6 \mathbf{i} + 2 \mathbf{j} + 4 \mathbf{k}$$

#### <mark style="background-color:red;">Resumo</mark>

Portanto, a soma dos vetores $$\mathbf{A} = (3, -2, 5)$$, $$\mathbf{B} = (4, 1, -3)$$ e $$\mathbf{C} = (-1, 3, 2)$$ resulta no vetor:

$$\mathbf{R} = (6, 2, 4)$$

***

{% embed url="https://portugol.dev/#share=dfd9m1d" %}
