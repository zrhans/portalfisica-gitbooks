const prompt = require("prompt-sync")({ sigint: true });

const PI = 3.14;
let h, d, y;
let f;

h = prompt("Digite o valor da altura (h) da coluna: ");
y = prompt("Digite o valor do peso específico (y) do volume: ");
d = prompt("Digite o valor do diâmetro (d) do cilindro: ");

f = (PI * y * (d * d) * h) / 4;

console.log("A força (f) sobre a válvula é: ", f, "N");

