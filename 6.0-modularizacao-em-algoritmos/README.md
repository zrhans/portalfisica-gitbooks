---
description: Suplemento de Aula
---

# 📖 6.0 Modularização em algoritmos

### &#x20;<a href="#id-6-0-modularizacao-em-algoritmos" id="id-6-0-modularizacao-em-algoritmos"></a>

_Fonte: Modularização na Lógica de Programação_ – [_blog.grancursosonline.com.br_](http://blog.grancursosonline.com.br/)

***

1. [6.0 Modularização em algoritmos ](https://portalfisica.com/fsc1189/6-0-modularizacao-em-algoritmos/#6-0-modularizacao-em-algoritmos)
2. [Modularização](https://portalfisica.com/fsc1189/6-0-modularizacao-em-algoritmos/#modularizacao)
3. [Material Extra](https://portalfisica.com/fsc1189/6-0-modularizacao-em-algoritmos/#material-extra)

### Modularização <a href="#modularizacao" id="modularizacao"></a>

**​​​​​​​**É a forma de dividir as tarefas ou algoritmos em **módulos**, também chamados subprogramas ou componentes.

Os módulos:

* Cuidam de uma parte separada de um problema;
* Realizam uma tarefa específica dentro duma solução do problema;
* São unidades funcionais de um algoritmo;
* São grupos de comandos com uma funcionalidade ou um objetivo bem definida; e
* Devem ser o mais independentes possível das outras partes do algoritmo.

Como características fundamentais, temos:

* Módulos: possuem um único ponto de entrada;
* Chamador ou invocador: tem sua execução suspensa durante a execução do módulo chamado;
* Controle de execução: retorna ao chamador quando a execução do módulo é finalizada.

As vantagens dos módulos são:

* Reduzem o tamanho do algoritmo;
* Melhoram a legibilidade do algoritmo;
* Evitam a repetição de código;
* Promove o reaproveitamento no mesmo programa ou em outros;
* Facilitam a manutenção e a divisão de trabalho; e
* Possibilitam a utilização de componentes prontos.

Podemos utilizar dois **tipos de módulos**:

* **Procedimentos** são módulo que não produzem um valor de saída;
  * Exemplos:
    * Imprimir o maior elemento de um grupo elementos;
    * Ler um elemento;
* **Funções** são módulos que produzem um valor de saída;
  * Exemplos:
    * Retornar o menor (ou maior) valor de um grupo de elementos;
    * Retornar um valor booleano caso um valor está dentro de um grupo de elementos.

> **Observação**
>
> Em algumas linguagens de programação, como Pascal, existem dois tipos:
>
> **procedimentos**, que não retornam valores
>
> **funções**, que retornam valores. Já em outras linguagens, como Linguagem C, todos os módulos são funções, que independente de retornarem ou não valores.
>
> — Devchannel

Exemplo de algortimo com um módulo em forma de função:

```
Algoritmo menorValor;
    var i, j: inteiro;
    função menorValor(m: inteiro, n: inteiro): inteiro
        var menor: inteiro;
        início da função
            menor := m;
            se (menor < n) então:
               menor := n;
            fim do se
            retorne(menor);
    fim da função
início do principal
    leia(i, j);
    escreva(“O menor valor é: ”, menorValor(i, j));
fim
```

**Questões de teste**

**\[CESPE 2011 EBC – Analista – Engenharia de Software**] julgue os itens subsequentes, referentes aos módulos que constituem a técnica de modularização, utilizada para desenvolver algoritmos.

Um procedimento ou sub-rotina é um conjunto de instruções que realiza determinada tarefa. Um algoritmo de procedimento, como qualquer outro algoritmo, deve ser identificado e pode possuir variáveis, operações e até funções.

**Comentários**:

Questão totalmente correta. Um módulo (sendo ele procedimento ou função) é um conjunto de instruções que realiza determinada tarefa.

Um algoritmo de um módulo, como qualquer outro algoritmo, deve ser identificado e pode possuir variáveis, operações e até funções.

Gabarito: **CERTO**.

**\[Quadrix 2018 SEDF – Professor Substituto – Informática]** No que se refere a procedimentos e funções, às estruturas de controle de fluxo nas linguagens de programação e à arquitetura J2EE, julgue o item que segue.

As funções podem ser utilizadas, em expressões, como se fossem variáveis.

**Comentários**:

Vamos pegar um exemplo de uma função chamada soma, que possui dois parâmetros.

No código principal do algoritmo, podemos ter:

```
var x, y: inteiro;
y := 10;
x := y;
x := soma(1, 2);
```

Notem que a varíavel x recebeu primeiro o valor da variável y. Depois, a variável x recebeu o retorno da função soma.

Gabarito: **CERTO**.

**\[CESPE 2008 SERPRO – Analista – Desenvolvimento de Sistemas]** Com relação às linguagens, julgue os itens a seguir.

As linguagens procedurais, como Cobol, Algol e C, têm como característica serem modulares e permitirem a reutilização de um mesmo código repetidamente em outras partes do programa, sem, necessariamente, copiá-lo.

**Comentários**:

Essas linguagens são modulares, ou seja, podemos criar módulos com elas, porém, repetindo: em algumas linguagens de programação, como Pascal, exitem dois tipos: procedimentos, que não retornam valores, e funções, que retornam valores. Já em outras linguagens, como Linguagem C, todos os módulos são funções, que independente de retornarem ou não valores.

Gabarito: **CERTO**.

**\[ESAF 2008 Prefeitura de Natal/RN – Auditor do Tesouro Municipal – Tecnologia da Informação – Prova 2 – Adaptada]** Analise as seguintes afirmações relacionadas a conceitos básicos sobre Programação:

\[I] Um procedimento é um conjunto de comandos para uma tarefa específica referenciada por um nome no algoritmo principal, retornando um determinado valor no seu próprio nome.

**Comentários**:

Podemos utilizar dois **tipos de módulos**:

* **Procedimentos** são módulo que não produzem um valor de saída;
  * Exemplos:
    * Imprimir o maior elemento de um grupo elementos;
    * Ler um elemento;
* **Funções** são módulos que produzem um valor de saída;
  * Exemplos:
    * Retornar o menor (ou maior) valor de um grupo de elementos;
    * Retornar um valor booleano caso um valor está dentro de um grupo de elementos.

O texto do item I citou função e não procedimento.

Gabarito: **ERRADO**.

**\[ESAF 2008 Prefeitura de Natal/RN – Auditor do Tesouro Municipal – Tecnologia da Informação – Prova 2 – Adaptada]** Analise as seguintes afirmações relacionadas a conceitos básicos sobre Programação:

\[II] Podem-se inserir módulos em um algoritmo. Para isso, pode-se utilizar “Procedimentos” ou “Funções”. As ações das “Funções” e dos “Procedimentos” são hierarquicamente subordinadas a um módulo principal.

**Comentários**:

Examente. Temos a área da criação dos módulos e esses módulos serão subornidados ao módulo principal, como vimos em nosso exemplo na teoria.

Gabarito: **CERTO**.

\[ESAF 2008 Prefeitura de Natal/RN – Auditor do Tesouro Municipal – Tecnologia da Informação – Prova 2 – Adaptada] Analise as seguintes afirmações relacionadas a conceitos básicos sobre Programação:

\[III] Cada “Função” ou “Procedimento” pode utilizar constantes ou variáveis do módulo principal ou definir suas próprias constantes ou variáveis.

**Comentários**:

Sim. Podemos ter constantes e variáveis locais dentro dos módulos.

Gabarito: **CERTO**.

<figure><img src="https://blog-static.infra.grancursosonline.com.br/wp-content/uploads/2022/04/27110047/Rogerio-Araujo-300x180-1.jpg" alt=""><figcaption></figcaption></figure>

Modularização na Lógica de Programação

blog.grancursosonline.com.br

***

Traga suas perguntas e dúvidas para o fórum e compartilhe com seus colegas ou verifique na seção perguntas e respostas se sua pergunta já não se encontra respondida lá.

**O que você aprendeu:**

– Funções

– Passagem de parâmetros

– Visibilidade (escopo) de variáveis (globais e locais)

– Bibliotecas

### Avaliação

### Material Extra <a href="#material-extra" id="material-extra"></a>

Assista os audivisuais e aprimores seus conhecimentos sobre o assunto.

{% embed url="https://youtu.be/y5MNhLo0Djc" %}
HM Programming
{% endembed %}

{% embed url="https://youtu.be/rs8ihN08bgU" %}
Hm Programming
{% endembed %}

