# 1 - Introdução

Esta vídeo aula aborda detalhes introdutórios sobre a norma ISO 5807 com maior aprofundidade em relação ao que já foi dito no canal. São apresentadas as categorias simbólicas abordadas na norma ISO 5807 e IBM que serão exploradas nas próximas aulas e devem ser utilizadas para a documentação no desenvolvimento de software. \
&#xNAN;_&#x41;utor:_  [_Augusto Manzano_](https://www.youtube.com/@am-42)&#x20;

{% embed url="https://youtu.be/T12e00hvV6o?si=1Q0ybQDH-RqCOjoe" %}



