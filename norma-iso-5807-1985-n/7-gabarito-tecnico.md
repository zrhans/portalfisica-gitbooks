# 7 - Gabarito técnico

Esta vídeo aula apresenta detalhes complementares a simbologia ISO e IBM. São apresentados os grupos de símbolos para a definição de fluxogramas e de diagramas de blocos, além de indicar os símbbolos básicos necessários a criação de diagramas de blocos.\
&#xNAN;_&#x41;utor:_  [_Augusto Manzano_](https://www.youtube.com/@am-42)&#x20;

{% embed url="https://youtu.be/imQde2YA0jM?si=q7yLh_Osk23JAQMV" %}

