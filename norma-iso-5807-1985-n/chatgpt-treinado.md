---
description: Exemplo de Algoritmo em Portugol usando a Norma ISO 5807
---

# ChatGPT treinado

Nesta página, apresentamos o resultado obtido com a IA, especificamente o ChatGPT, após a solicitação de um exemplo de algoritmo em Portugol conforme a Norma ISO 5807. Inicialmente, foram recebidas respostas inconsistentes, mas com a intervenção do professor (treinador), a IA conseguiu aprender e estruturar corretamente o exemplo usando a sintaxe adequada.

***

O exemplo anterior já incluía a potenciação, mas vamos reforçar e garantir que tudo esteja claro, apresentando novamente o código em **Portugol** que realiza as operações matemáticas básicas, incluindo a potenciação, com todos os operadores corretos conforme a **Norma ISO 5807:1985(E)**.

Aqui está o código atualizado:

```scss
// Algoritmo: Operações Matemáticas Básicas
// Objetivo: Realizar operações matemáticas simples com dois números
// Entrada: Dois números fornecidos pelo usuário
// Saída: Resultado das operações (soma, subtração, multiplicação, divisão, exponenciação)

Inicio

    // Declaração de variáveis
    inteiro numero1, numero2
    real soma, subtracao, multiplicacao, divisao, potencia

    // Entrada de dados
    escreva("Digite o primeiro número: ")
    leia(numero1)
    escreva("Digite o segundo número: ")
    leia(numero2)

    // Processamento das operações matemáticas
    soma ← numero1 + numero2
    subtracao ← numero1 - numero2
    multiplicacao ← numero1 ⊗ numero2  // Usando o operador correto de multiplicação (⊗)

    // Verificação para evitar divisão por zero
    se numero2 <> 0 entao
        divisao ← numero1 / numero2
    senao
        escreva("Não é possível dividir por zero.\n")
    fimse

    potencia ← numero1 ↑ numero2  // Usando o operador correto de exponenciação (↑)

    // Saída dos resultados
    escreva("Soma: ", soma, "\n")
    escreva("Subtração: ", subtracao, "\n")
    escreva("Multiplicação: ", multiplicacao, "\n")

    // Exibição da divisão apenas se número2 for diferente de zero
    se numero2 <> 0 entao
        escreva("Divisão: ", divisao, "\n")
    fimse

    escreva("Potência: ", potencia, "\n")

Fim
```

#### Detalhes das Operações:

1. **Soma**: `soma ← numero1 + numero2`
   * Realiza a soma dos dois números.
2. **Subtração**: `subtracao ← numero1 - numero2`
   * Realiza a subtração do segundo número pelo primeiro.
3. **Multiplicação**: `multiplicacao ← numero1 ⊗ numero2`
   * Utiliza o símbolo de multiplicação correto da norma ISO.
4. **Divisão**:
   * Verifica se `numero2` é diferente de zero antes de realizar a divisão para evitar erros.
   * `divisao ← numero1 / numero2`.
5. **Exponenciação**: `potencia ← numero1 ↑ numero2`
   * Utiliza o símbolo correto para a potenciação conforme a norma.

#### Exemplo de Entrada e Saída

Se o usuário digitar:

* **Número 1**: 2
* **Número 2**: 3

A saída será:

```
Soma: 5
Subtração: -1
Multiplicação: 6
Divisão: 0.6667
Potência: 8
```

Agora o código está completo, demonstrando todas as operações matemáticas básicas, incluindo a potenciação. Se você precisar de mais assistência ou esclarecimentos, sinta-se à vontade para perguntar para o professor!

#### Implementação no Portugol Webstudio

{% embed url="https://portugol.dev/#share=uhab2dz" %}
