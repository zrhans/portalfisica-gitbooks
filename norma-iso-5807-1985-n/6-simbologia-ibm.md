# 6 - Simbologia IBM

Esta vídeo aula apresenta detalhes sobre a categoria de símbolos documenrados pela IBM. São apresentados os símbolos IBM e ISO indicados pela própria IBM e não definidos na norma ISO 5807.\
&#xNAN;_&#x41;utor:_  [_Augusto Manzano_](https://www.youtube.com/@am-42)&#x20;

{% embed url="https://youtu.be/s1iyvs9fCTQ?si=RxzMLKOgS0F85kNI" %}

