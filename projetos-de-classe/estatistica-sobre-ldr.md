---
description: Projeto 1
---

# Estatística sobre LDR

Elaborar um projeto prático de demonstração de funcionamento de um LDR.

a) Elaborar diagramas de montagem e o algoritmo para informar em intervalos de tempos periódicos (o usuário escolhe ao rodar o programa) quais foram os parâmentros estatísticos das medidas de tensão lidos (a cada segundo) nos terminais do LDR:

1. Média
2. Máxima e Mínima
3. Variância e o Desvio Padrão

#### Por onde começar?

[https://www.makerhero.com/blog/o-que-e-ldr/?utm\_source=ActiveCampaign\&utm\_medium=email\&utm\_content=Confira%20as%20novidades%20e%20destaques%20desse%20m%C3%AAs%21\&utm\_campaign=Newsletter%20%7C%20Outubro](https://www.makerhero.com/blog/o-que-e-ldr/?utm\_source=ActiveCampaign\&utm\_medium=email\&utm\_content=Confira%20as%20novidades%20e%20destaques%20desse%20m%C3%AAs%21\&utm\_campaign=Newsletter%20%7C%20Outubro)
