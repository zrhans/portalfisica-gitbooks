---
description: Ferramentas de Programação online
---

# Ferramentas Online

Use us ferramentas online disponíveis.

Implemente seus códigos em Python eou JavaScript

***

😎 Você tem meu convite para assistir meus códigos na VM CodeSandBox: [https://codesandbox.io/invite/73hqc5kd8eg0sbbe](https://codesandbox.io/invite/73hqc5kd8eg0sbbe)

***

{% @gitlab-files/gitlab-code-block url="https://gitlab.com/-/snippets/3753632" %}

{% @gitlab-files/gitlab-code-block url="https://gitlab.com/snippets/3753632.git" %}

{% @gitlab-files/gitlab-code-block url="https://gitlab.com/-/snippets/3753632/raw/main/fazenda.js" %}

{% @gitlab-files/gitlab-code-block url="https://gitlab.com/-/snippets/3753632" %}

