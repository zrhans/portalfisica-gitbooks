# Uma visão geral sobre lógica de programação age

Fonte: https://manoelcampos.gitbooks.io/logica-programacao/content

A lógica de programação é a disciplina que ensina os **fundamentos** para o desenvolvimento de programas em geral, a serem utilizados para as mais diversas finalidades e dispositivos, como apresentado no capítulo anterior.

Programas são desenvolvidos visando um determinado objetivo, seja para resolução de problemas ou apenas entretenimento (como no caso dos jogos).

**Funcionamento de programas e suas utilidades**

Pense em um programa como um robô: ele não faz nada que não tenha sido programado para fazer. Se ele faz algo que não era esperado, pode ser por motivo de falhas mecânicas ou humanas ao programá-lo. Mesmo robôs que tomam decisões autônomas o fazem com base em análise de dados e instruções programadas por humanos. Por exemplo, sem precisar ir muito longe até os carros que dirigem sozinhos, atualmente temos carros convencionais, guiados por pessoas, que detectam a iminência de uma colisão e acionam o freio automaticamente. O veículo analisa dados como imagens de câmeras, velocidade do veículo e distância até um obstáculo. A partir de resultados de cálculos, o computador de bordo aciona os freios. Sem o conhecimento humano para desenvolver os programas utilizados pelo carro, nada disso seria possível.

Utilizando técnicas de inteligência artificial, o programa pode aprender ao longo do tempo a detectar situações não esperadas. Por exemplo, um programa que detecta pessoas em uma foto por melhorar a medida que ele avalia diferentes fotos de uma mesma pessoa, assim como acontece redes sociais como o Facebook.

{% embed url="https://youtu.be/ig54q0rG94s" %}
Estacionamento autônomo de veículo, sem presença do motorista
{% endembed %}



> Tecnologias como a utilizada em carros para o sistema de estacionamento autônomo normalmente começam com protótipos em computadores convencionais. Um exemplo é [este aplicativo](http://rorchard.github.io/FuzzyJ/FuzzyTruck.html) que permite estacionar um caminhão em uma vaga de garagem. Você pode brincar com o aplicativo, que pode ser acessado online no final da página indicada.
>
> Clique em qualquer lugar do retângulo para reposicionar o caminhão, altere o "_Truck Angle_" para mudar o ângulo inicial do veículo, diminua o "_Simulation Speed_" e clique em "_Go_" para ver o sistema funcionando.

**Como desenvolver programas**

As instruções dadas a um programa de computador são organizadas como em uma lista de tarefas ou uma receita de bolo. Desenvolver um programa é como produzir um filme, onde você é o diretor/roteirista e o programa é um ator executando as instruções como definidas no roteiro. Muitas vezes o ator pode improvisar, assim como um programa pode tomar decisões autônomas, como no caso do exemplo dos veículos acima.

Assim como o roteiro de um filme é escrito utilizando alguma linguagem natural como inglês ou português, o "roteiro" de um programa também, só que utilizando-se linguagens específicas chamadas **linguagens de programação**. Linguagens naturais muitas vezes causam ambiguidades e não são concisas.

<figure><img src="../.gitbook/assets/image (29).png" alt=""><figcaption><p>Figura 1. Ambiguidade no português</p></figcaption></figure>



<figure><img src="../.gitbook/assets/image (30).png" alt=""><figcaption><p>Figura 2. Ambiguidade na vida de um programador</p></figcaption></figure>



| Extenso                                                                                      | Conciso                                           |
| -------------------------------------------------------------------------------------------- | ------------------------------------------------- |
| A multiplicidade de funcionalidades é realmente vantajosa para a comercialização do produto. | As várias funções do produto ajudarão nas vendas. |

As linguagems de programação são então utilizadas para tornar mais fácil a tarefa de desenvolver programas. Existem inúmeras linguagens de programação, com diversos níveis de complexidade, para as mais variadas finalidades. Linguagens naturais como alemão podem ser mais difíceis de aprender do que outras como inglês. Elas foram evoluindo ao longo dos séculos, se adaptando a aspectos culturais e às necessidades da sociedade para tornar a comunicação mais eficiente. Palavras e expressões caem em desuso e novas expressões são incorporadas ao idioma. Determinados idiomas são derivados de outros, como por exemplo o português e espanhol que são derivados do latim.

Tudo isso se aplica às linguagens de programação. Existem linguagens de [A a Z](https://en.wikipedia.org/wiki/List_of_programming_languages), apesar de poucas delas serem amplamente utilizadas. O processo de aprendizagem de uma linguagem de programação é semelhante ao de qualquer idioma: a partir do momento que se conhece uma determinada linguagem, para aprender outra será bem mais fácil, principalmente se esta for derivada da que você já conhece.

Linguagens de programação são em sua maioria derivadas do inglês. O aprendizado inclui vocabulário, gramática e outros detalhes. Felizmente, o vocabulário destas linguagens é extremamente sucinto, contendo apenas algumas dezenas de palavras.

**Linguagens de programação e algoritmos**

Como já sabemos, programas são desenvolvidos a partir da escrita de um "roteiro" em uma **linguagem de programação** qualquer. Tal roteiro é denominado **algoritmo**. Ele é como uma redação, que precisa conter uma estrutura com começo, meio e fim. Ele deve seguir determinados padrões como pontuação e separação de parágrafos, tanto por questões de clareza e organização quanto para dar sentido ao texto.

> Um algoritmo é um conjunto finito de instruções/comandos não ambíguos, escrito em uma determinada linguagem, para resolver um tipo de problema.

Pode-se utilizar qualquer linguagem de programação para escrever algoritmos. Eles são a fonte para a criação de programas. Algoritmos representam soluções computacionais para um determinado tipo de problema, ou seja, soluções que podem ser executadas por um computador.

Um algoritmo pode ser escrito por qualquer pessoa com conhecimentos de lógica de programação e do problema a ser resolvido. Diferentes pessoas normalmente encontram diferentes soluções para um mesmo problema. Existem diversos problemas clássicos que podem ser resolvidos por meio de um algoritmo como o:

[![tsp.png](https://ufsm.gitbook.io/~gitbook/image?url=https%3A%2F%2Fmanoelcampos.gitbooks.io%2Flogica-programacao%2Fcontent%2Fimages%2Ftsp.png\&width=300\&dpr=4\&quality=100\&sign=d203259a\&sv=1)](https://optimization.mccormick.northwestern.edu/index.php/Traveling_salesman_problems) [Problema do Caixeiro Viajante](https://pt.wikipedia.org/wiki/Problema_do_caixeiro-viajante)

que visa encontrar a menor distância entre dois pontos. Alguns exemplos de tal problema incluem encontrar a melhor rota para um caminhão de entregas, o melhor itinerário para ônibus em uma cidade [\[TSP\]](https://manoelcampos.gitbooks.io/logica-programacao/content/chapter1.html#TSP), a menor distância para enviar uma mensagem entre dois computadores na internet, etc.

💡Você pode testar uma solução para o Problema do Caixeiro Viajante [nesta página](http://examples.gurobi.com/traveling-salesman-problem/#demo). Basta clicar nas cidades no mapa e depois no botão "Compute Tour".

{% embed url="https://youtu.be/SC5CX8drAtU" %}
Problema do Caixeiro Viajante: encontrando a menor rota para visitar uma lista de cidades
{% endembed %}



* [![knapsack.png](https://ufsm.gitbook.io/~gitbook/image?url=https%3A%2F%2Fmanoelcampos.gitbooks.io%2Flogica-programacao%2Fcontent%2Fimages%2Fknapsack.png\&width=300\&dpr=4\&quality=100\&sign=65477f6d\&sv=1)](https://pt.wikipedia.org/wiki/Problema_da_mochila)[Problema da Mochila](https://pt.wikipedia.org/wiki/Problema_da_mochila)
* Visa colocar o máximo de produtos dentro de uma mochila de modo a obter o maior valor total em produtos (cada produto tem um preço). Algumas aplicações reais do problema da mochila incluem [\[KP\]](https://manoelcampos.gitbooks.io/logica-programacao/content/chapter1.html#KP):
  * **transporte de carga**: carregamento de conteiners em navios de carga;
  * **investimento no mercado financeiro**: considerando que tem-se uma determinada quantidade de dinheiro para investir, pode-se aplicar o problema da mochila para selecionar os melhores investimentos que conjuntamente aumentem os lucros, com o menor custo ou risco de investimento;
  * **computação na nuvem**: aplicações como o Google Fotos permitem fazer o upload de fotos em smartphones para a nuvem, aplicando filtros e gerando vídeos automaticamente a partir das fotos enviadas. Diferente de aplicativos como o Instagram, filtros são aplicados automaticamente em fotos selecionadas, realizando o processamento do filtro nos computadores na nuvem e não no smartphone do usuário. O Google Fotos possui milhões de usuários, necessitando de milhares de computadores para processar as fotos. Como computadores tem capacidade limitada, o problema da mochila pode ser usado nestes contextos para selecionar adequadamente quais fotos serão processadas por quais computadores.

#### Representação de algoritmos <a href="#representacao" id="representacao"></a>

Imagine que você é um programador e deseja programar um robô para testar e trocar lâmpadas na sua casa. Você precisará então elaborar um algoritmo para isto. Algoritmos podem ser representados de diversas maneiras. Uma maneira simples, e que facilita o entendimento, é por meio de fluxogramas como o da figura abaixo.

💡Comprar um robô e programar ele você mesmo é uma realidade. Existem diversos fabricantes vendendo desde robôs de brinquedo como da [Lego](https://www.apple.com/swift/playgrounds), até robôs para resolver problemas reais.

<figure><img src="../.gitbook/assets/image (31).png" alt=""><figcaption><p>Figura 3. Fluxograma para troca de lâmpada</p></figcaption></figure>

Fluxogramas são amplamente utilizados em diversas áreas para representar, por exemplo, o fluxo de:

* processos em uma instituição;
* tomada de decisões em uma empresa;
* entrega de produtos comprados pela internet, etc...

A figura apresentada mostra os passos básicos que o robô deve seguir (as instruções que ele deve executar) para verificar se a lâmpada está funcionando e realizar a troca. O algoritmo inicia na instrução "Acionar interruptor" e segue o fluxo definido pelas setas, de acordo com as respostas para as perguntas ao longo do processo.

Este é um algoritmo apenas para efeitos didáticos, pois ele não cobre uma série de situações. Por exemplo, se após ser colocada uma nova lâmpada esta não acender, o algoritmo vai instruir o robô a chamar um eletricista. No entanto, esta nova lâmpada pode estar apenas queimada e não ser preciso chamar um eletricista.

Fluxogramas podem ser utilizados para representar mais claramente o funcionamento de um algoritmo para uma equipe de programadores empenhados em resolver um problema. Ferramentas como o [Scratch](http://scratch.mit.edu/) permitem ainda criar algoritmos a partir da montagem de blocos. O vídeo abaixo exemplifica como é possível animar um personagem em um jogo a partir de um algoritmo construído no Scratch.

{% embed url="https://youtu.be/pljz7BdQAts" %}
Exemplo de construção de algoritmos utilizando blocos com o editor Scratch - [http://scratch.mit.edu](http://scratch.mit.edu/)
{% endembed %}



Para a animação do personagem, existem diversas imagens que são apresentadas ao longo do tempo, cada uma com o personagem em uma pose levemente diferente, como é possível perceber na figura abaixo. O algoritmo então se encarrega de realizar a troca das imagens com um determinado intervalo de tempo.

<figure><img src="../.gitbook/assets/image (33).png" alt=""><figcaption><p>Figura 4. Método básico de animação de personagens em jogos: utilização de várias imagens com diferentes poses.</p></figcaption></figure>

Criar programas a partir de fluxogramas e blocos é normalmente feito apenas na faze de aprendizagem. A partir do momento que você ganhar desenvoltura na escrita de algoritmos, verá que ferramentas como estas são bastante limitadas para serem utilizadas profissionalmente.

Algortimos são comumente representados de forma textual, utilizando-se alguma linguagem de programação ou pseudo-código: uma linguagem fictícia, utilizada normalmente apenas para aprendizagem, e que pode ser baseada em um idioma como o português.

Para a animação apresentada no vídeo acima, um trecho do algoritmo desenvolvido em blocos poderia ser escrito em pseudo-código como demonstrado seguir.

Trecho de algoritmo para animação de personagem em jogo

Copy

```
mostre imagem Run_000.png
repita 9 vezes
    mova 4 posições
    mostre próxima imagem
    aguarde 0.1 segundo

repita 9 vezes
    mostre próxima imagem
    aguarde 0.1 segundo
```

Pseudo-códigos podem ser escritos em uma pseudo-linguagem \[[2](https://manoelcampos.gitbooks.io/logica-programacao/content/chapter1.html#_footnote_2)] como o [Portugol](https://pt.wikipedia.org/wiki/Portugol). Apesar de não ser uma linguagem "real" \[[3](https://manoelcampos.gitbooks.io/logica-programacao/content/chapter1.html#_footnote_3)], existem algumas ferramentas que de fato geram programas a partir deste pseudo-código, como é o caso do [Portugol Studio](http://lite.acad.univali.br/portugol).

#### Estrutura de um Algoritmo <a href="#estrutura_de_um_algoritmo" id="estrutura_de_um_algoritmo"></a>

Imagine que você é o chefe em um departamento de uma empresa e você precisa que um de seus funcionários execute alguma tarefa, como comprar passagens aéreas para uma viagem de negócios. Para que o funcionário possa fazer isso, você deve fornecer alguns dados para ele. Por exemplo, você precisa informar para qual cidade você vai viajar e a data de ida e volta. Sem tais dados, o funcionário não terá como realizar a compra das passagens. Estes são dados essenciais para a realização da tarefa.

O funcionário precisará então usar tais dados para localizar as passagens aéreas mais adequadas para o chefe. Ele pode então utilizar o computador para pesquisar as passagens e encontrar o menor preço.

Por fim, depois que o funcionário escolheu quais as melhores passagens áreas, ele deve então informar ao chefe quais foram as passagens localizadas, horários dos voos e outros dados.

Observe que o processo descrito acima é dividido em 3 etapas que são denominadas:

* **entrada**: o chefe informa as datas da viagem para o funcionário;
* **processamento**: o funcionário usa o computador para localizar passagens aéreas mais baratas para as datas informadas;
* **saída**: o funcionário informa o chefe sobre as passagens localizadas, horários, preços, etc.

<figure><img src="../.gitbook/assets/image (34).png" alt=""><figcaption><p>Figura 5. Estrutura convencional de um algoritmo</p></figcaption></figure>

Da mesma forma, algoritmos normalmente possuem uma estrutura dividida nestas 3 partes. Eles são como uma redação contendo introdução, desenvolvimento e conclusão. Um programa então pode receber dados de entrada, executar operações com estes dados, e exibir um resultado para o usuário.

É importante conhecer esta divisão ao construir algoritmos pois é uma forma de entender por onde começar e como conduzir o desenvolvimento. Você primeiro tem que pensar em quais entradas o usuário precisará informar ao programa. A entrada de dados é opcional e depende do problema que está sendo desenvolvido, mas normalmente os programas precisam de uma entrada de dados.

Após ter pensado em quais dados precisam ser passados para o programa, você deve continuar a escrita do seu algoritmo focando em como você escreverá a parte do algoritmo para processar os dados de entrada. Essa é a parte principal e normalmente mais complexa. O processamento é onde está a maior parte do trabalho de um programa. Esta parte é onde os problemas para os quais o problema foi desenvolvido serão resolvidos.

Por fim, após ter escrito a parte do algoritmo que vai processar os dados de entrada, já está quase tudo pronto. Basta agora exibir os resultados para o usuário. Esta é a saída de dados. Assim, você terá desenvolvido seu algoritmo.

Se você relembar o Problema do Caixeiro Vijante, onde um programa deve encontrar a menor rota para visitar um conjunto de cidades, temos:

* a lista de cidades a serem visitadas como **entrada**;
* o programa **processa** essa lista para encontrar a melhor rota;
* por fim, ele exibe a melhor rota encontrada, mostrando a ordem em que as cidades podem ser visitadas como **saída**.

No caso de uma receita de bolo, temos o mesmo padrão: a entrada são os ingredientes, o processamento é a preparação do bolo, e a saída é o bolo pronto.

<figure><img src="../.gitbook/assets/image (36).png" alt=""><figcaption><p>Figura 6. Entrada, processamento e saída ao executar uma receita de bolo - <em>Fonte:</em> <a href="http://web.liberty.k12.mo.us/hs/lhswebsite/klein/computer-literacy.html"><em>web.liberty.k12.mo.us</em></a></p></figcaption></figure>

Para cada etapa de um programa a ser criado, precisamos indicar no nosso algoritmo as instruções para que o programa faça o que pretendemos. Se desejarmos que o programa receba dados de entrada, precisamos utilizar comandos capazes de obter tais dados.

Se você já estudou introdução à ciência da computação, sabe que o computador tem diferentes [dispositivos de entrada e saída de dados](https://pt.wikipedia.org/wiki/Entrada/sa%C3%ADda), chamados de **periféricos**, como mostra a figura abaixo.

<figure><img src="../.gitbook/assets/image (37).png" alt=""><figcaption><p>Figura 7. Dispositivos de Entrada/Saída - <em>Fonte:</em> <a href="https://www.thinglink.com/scene/725787867065876481"><em>thinglink.com</em></a></p></figcaption></figure>

O teclado é o dispositivo mais utilizado para entrada de dados em um computador convencional (desktop ou notebook), além de outros como mouse, microfone, webcam, monitores touchscreen (sensível ao toque), pendrive, CD. Qualquer um desses dispositivos pode ser utilizado para enviar dados para um computador, para que sejam processados por um programa.

O processador é responsável por processar os dados capturados por dispositivos de entrada, normalmente realizando cálculos e operações com os dados recebidos. Por exemplo, se uma foto em um pendrive foi utilizada como dado de entrada, tal foto pode ser convertida para preto e branco na fase de processamento.

<figure><img src="../.gitbook/assets/image (39).png" alt=""><figcaption><p>Figura 8. Conversão de imagem para preto e branco - <em>Fonte:</em> <a href="https://coderwall.com/p/kc2ftg/css3-filters%E2%80%94%E2%80%8B2"><em>coderwall.com</em></a></p></figcaption></figure>

Dispositivos de saída como monitor, impressora, pendrive e CD podem ser utilizados para exibir os dados depois de processados. A foto em preto e branco exibida no monitor seria um exemplo de saída. No entanto, no lugar de automaticamente exibir a foto, ela poderia simplesmente ser salva em um arquivo num pendrive.

💡Observe que há dispositivos que podem funcionar como entrada ou saída, como é o caso de minotores touchscreen, pendrives e CDs.

#### Resumo do Capítulo <a href="#resumo_do_cap_tulo" id="resumo_do_cap_tulo"></a>

1. **Lógica de programação** ensina os fundamentos para **desenvolvimento de programas**.
2. Programas convencionais só fazem aquilo que foram programados por humanos para fazer.
3. Desenvolver um programa é como escrever um **roteiro** de filme ou uma **receita** de bolo: um conjunto de **instruções** é definido para serem seguidos.
4. O "roteiro" para a elaboração de um programa é escrito utilizando-se linguagens específicas chamadas **linguagens de programação**.
5. Linguagens naturais como português geram ambiguidades e não são concisas.
6. Linguages de programação tornam mais fácil o desenvolvimento de programas.
7. Linguagens de programação são normalmente baseadas no inglês e também podem derivar umas das outras.
8. Existem inúmeras linguagens de programação, com os mais diversos níveis de complexidade e para as mais variadas finalidades.
9. O "roteiro" ou "receita" de um programa é formalmente chamado de **algoritmo**.
10. Algoritmos são elaborados para resolver um tipo de problema. A partir deles são criados programas.
11. Existem diversos problemas conhecidos que podem ser resolvidos por meio de algoritmos, como o Problema do Caixeiro Viajante, Problema da Mochila e outros.
12. Tais problemas, apesar de parecem bobos, têm várias aplicações no mundo real.
13. Algoritmos podem ser representados de diferentes maneiras: graficamente com o uso de **fluxogramas** e **blocos**; ou por meio de **pseudo-código** ou **código** escrito em uma **linguagem de programação**.
14. Estrutura de um algoritmo. Dispositivos de entrada e saída.

#### Lições a serem tomadas <a href="#li_es_a_serem_tomadas" id="li_es_a_serem_tomadas"></a>

> Quando você aprende a ler, você lê para aprender. Quando você aprende a programar, você programa para aprender.
>
> — Mitch Resnick _Criador da ferramenta Scratch, utilizada para aprendizagem de programação._

Escrever algoritmos como os apresentados até agora é fácil. Mas de fato, criar algoritmos para resolver problemas reais como os vários apresentados ao longo do capítulo é desafiador. Mas com dedicação, qualquer pessoa pode aprender a desenvolver algoritmos.

O vídeo abaixo é uma apresentação sobre a ferramenta Scratch, que é bastante utilizada para ensinar crianças a programar, principalmente pela criação de jogos. O vídeo inicia nos 13 minutos, e conta um trecho da história de um garoto que estava aprendendo a criar jogos no Scratch. **Ative as legendas e veja as várias lições a serem aprendidas.**

Aprender a elaborar algoritmos vai muito além de criar programas.

{% embed url="https://youtu.be/Ok6LbV6bqaE" %}



Aprender a programar lhe trará inúmeros benefícios tanto profissionalmente como na sua vida pessoal. Tendo que pensar em inúmeras possibilidades e cenários ao desenvolver algoritmos lhe fará uma pessoa mais observadora e crítica.

Com interesse, você começará a ver o mundo com outros olhos. Isto pode despertar seu espírito empreendedor, procurando identificar problemas em todo lugar. Por fim, se o Thomas (um garoto de 12 anos) pode, você também pode.

Um desenvolvedor de aplicativos de 12 anos de idade. (**Ative as legendas**)

{% embed url="https://youtu.be/Fkd9TWUtFm0" %}
Um desenvolvedor de aplicativos de 12 anos de idade
{% endembed %}

### Exercícios

1\) Escreva um algoritmo em pseudo-código, contendo as instruções necessárias para um robô realizar a troca do pneu de um carro. Um algoritmo de exemplo para a troca de uma lâmpada é apresentado abaixo:

Algoritmo de troca de lâmpada

```
Acionar interruptor
Se a lâmpada acender então (1)
   Diga "A lâmpada está funcionando."
senão
    Se já trocou a lâmpada então
        Chamar eletricista
        Fim
    senão
        Girar lâmpada no sentido anti-horário
        Remover lâmpada antiga
        Colocar nova lâmpada
        Girar nova lâmpada no sentido horário
        Voltar para passo (1)
```

2\) Tente identificar as falhas do seu algoritmo de troca de pneu, como as condições que você não verificou. Por exemplo, você verificou se o pneu reserva (o _step_) está vazio?

3\) Compare o algoritmo que você desenvolveu com o de um colega e veja onde o seu algoritmo pode melhorar e onde o dele pode ser melhorado.

#### Referências <a href="#refer_ncias" id="refer_ncias"></a>

* \[TSP] Rajesh Matai, Surya Singh and Murari Lal Mittal. Traveling Salesman Problem: an Overview of Applications, Formulations, and Solution Approaches. InTech. 2010. [http://doi.org/10.5772/12909](http://doi.org/10.5772/12909)
* \[KP] John J. Bartholdi, III. The Knapsack Problem. Springer. [https://doi.org/10.1007/978-0-387-73699-0\_2](https://doi.org/10.1007/978-0-387-73699-0_2)

***

[1](https://manoelcampos.gitbooks.io/logica-programacao/content/chapter1.html#_footnoteref_1). excessivas, redundantes[2](https://manoelcampos.gitbooks.io/logica-programacao/content/chapter1.html#_footnoteref_2). Linguagem fictícia[3](https://manoelcampos.gitbooks.io/logica-programacao/content/chapter1.html#_footnoteref_3). Pseudo linguagens como Portugol não são utilizadas profissionalmente

